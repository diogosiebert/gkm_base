MODULE update_sub
	
USE gkm_fluxes 
USE EM
USE function_module, ONLY: COURANT_HALL
USE boundary_cond

IMPLICIT Double Precision (A-H,O-Z)

CONTAINS

	Subroutine UPDATE(TAU,TAU_H,alpha,Cell,gc,CK,GAS_CONSTANT,Pr,TEMPchar,&
			DENSchar,gradP,VISC,IX,&
	                  temp,DENS,XMOM,YMOM,ZMOM,ENER,&
	                  BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,&
			  XMOM2,YMOM2,ZMOM2,ENER2,BIX2,BIY2,BIZ2,PSI2,&
			  Bmu_0,COND,B0,Uchar, &
	                  IT,Lchar,MOL_MASS,Avag,Q_elec,kappa, &
	                  HALLMHD,&
			  CFL,SSIT,vmax,vmaxh,c_ratio,ch_mult,periods)
			  
!--------------------------------------------------------------------------------
	Integer :: gc,SSIT
	Logical :: HALLMHD,periods(0:2)
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,XMOM,&
	             YMOM,ZMOM,ENER,FM,FPX,FPY,FPZ,FE,GM,GPY,GPX,GPZ,GE,HM,HPZ,HPY,HPX,HE,temp,&
	                            BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
				    XMOM2,YMOM2,ZMOM2,ENER2,BIX2,BIY2,BIZ2,&
				    BIXH,BIYH,BIZH,PSIH,BIXTEMP,BIYTEMP,BIZTEMP,PSITEMP,PSI2,&
				    FPSI,FBEVOX,FBEVOY,FBEVOZ,&
				    GPSI,GBEVOX,GBEVOY,GBEVOZ,&
				    HPSI,HBEVOX,HBEVOY,HBEVOZ,BPSI,PSIBX,PSIBY,PSIBZ
	
	Double Precision :: MOL_MASS, Q_elec, Avag, kappa,MAX_THALL,Lchar
	CellI=1.D0/Cell

!-------DXEE, etc. Subroutines calculated fluxes according to regular GKM
!-------You will not need to change these yet


	Call DXEE(TAU,Cell,gc,CK,GAS_CONSTANT,Pr,TEMP_MEAN,VISC,IX,&    !2nd order fluxes
	          temp,DENS,XMOM,YMOM,ZMOM,ENER,FM,FPX,FPY,FPZ,FE)

!! HALL SUB-CYCLING FOR 1st ORDER SCHEME
   !If (HALLMHD) then
   !SSIT = 0
   !TIME_HALL = 0.D0
   !MAX_THALL = TIME_HALL + TAU
   !TAU_eps = 1.D-10
	!Do 
	  !If (TIME_HALL.gt.MAX_THALL) Exit
	    !if((TIME_HALL+TAU_H).gt.(MAX_THALL)) then
	      !TAU_H = (1.D0+TAU_eps)*MAX_THALL-TIME_HALL
	    !end if
	  !TIME_HALL = TIME_HALL + TAU_H
	  !SSIT = SSIT + 1

	  !Call Hall_MHD(Cell,gc,IX,DENS,XMOM,YMOM,ZMOM,ENER,&
	           !BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
		   !FBEVOX,FBEVOY,FBEVOZ,GBEVOX,GBEVOY,GBEVOZ,HBEVOX,HBEVOY,HBEVOZ,&
		   !BPSI,PSIBX,PSIBY,PSIBZ,FPSI,GPSI,HPSI,&
	           !Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	           !temp, kappa,vmaxh,c_ratio,ch_mult)

	      !Do i=1,IX
	      !PSI(i) = PSI(i)&
			    !+((FPSI(i-1)-FPSI(i)& !MHD
			    !)*CellI&
			   !+BPSI(i))*TAU_H

	        !BIX(i) = BIX(i)&      ! X-Magnetic Field Evolution
			    !+((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    !)*CellI&
			    !+PSIBX(i))*TAU_H
											 
	        !BIY(i) = BIY(i)&      ! Y-Magnetic Field Evolution
			    !+((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    !)*CellI&
			    !+PSIBY(i))*TAU_H
						  
	        !BIZ(i) = BIZ(i)&      ! Z-Magnetic Field Evolution
			    !+((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    !)*CellI&
			    !+PSIBZ(i))*TAU_H
	      !End Do

          !Call BOUNDARY_HALL(IX, &
	                    !Lchar,Cell,gc,&
	                    !BIX,BIY,BIZ,PSI,B0)
	!EndDo
   !EndIf
   !!----END HALL SUB-CYCLING ----


!-------Mhd subroutine which calculates contributions of Mhd to the Fluxes
	Call MHD(Cell,gc,CK,IX,DENS,XMOM,YMOM,ZMOM,ENER,&
	         BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
				    BPSI,PSIBX,PSIBY,PSIBZ,&
				    FPSI,FBEVOX,FBEVOY,FBEVOZ,&
				    GPSI,GBEVOX,GBEVOY,GBEVOZ,&
				    HPSI,HBEVOX,HBEVOY,HBEVOZ,&
	         Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	         temp, kappa,TAU,&
	         HALLMHD,vmax,c_ratio,ch_mult)
				 
	    Do i=1,IX

!################################################################
!!#### 1st ORDER ##########
	      !PSI(i) = PSI(i)&
			    !+(FPSI(i-1)-FPSI(i)& !MHD
			    !)*CellI*TAU&
			   !+BPSI(i)*TAU
						  
	      !DENS(i) = DENS(i)&
	                    !+(FM(i)-FM(i+1)&
	                   !)*CellI
        
	      !XMOM(i) = XMOM(i)&
	                    !+(FPX(i)-FPX(i+1)&
	                    !)*CellI&
	                    !+(BXM(i))*TAU !Mhd Contribution
							  
	      !YMOM(i) = YMOM(i)&
	                    !+(FPY(i)-FPY(i+1)&
	                    !)*CellI&
	                    !+(BYM(i))*TAU !Mhd Contribution
				  
	      !ZMOM(i) = ZMOM(i)&
	                    !+(FPZ(i)-FPZ(i+1)&
	                    !)*CellI&
	                    !+(BZM(i))*TAU !Mhd Contribution

	      !BIX(i) = BIX(i)&      ! X-Magnetic Field Evolution
			    !+((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    !)*CellI+PSIBX(i)&
			    !+BEVOX(i))*TAU
											 
	      !BIY(i) = BIY(i)&      ! Y-Magnetic Field Evolution
			    !+((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    !)*CellI+PSIBY(i)&
			    !+BEVOY(i))*TAU
						  
	      !BIZ(i) = BIZ(i)&      ! Z-Magnetic Field Evolution
			    !+((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    !)*CellI+PSIBZ(i)&
			    !+BEVOZ(i))*TAU

	      !ENER(i) = ENER(i)& 
	                    !+(FE(i)-FE(i+1)&
	                    !)*CellI&
	                    !+(BEN(i))*TAU  !Mhd Contribution

!!#### end 1st order #######

!#### RK2 1st step ##########
	      PSI2(i) = PSI(i)&
			    +(FPSI(i-1)-FPSI(i)& !MHD
			    )*CellI*TAU&
			   +BPSI(i)*TAU
						  
	      DENS(i) = DENS(i)&
	                    +(FM(i)-FM(i+1)&
	                    )*CellI
        
	      XMOM2(i) = XMOM(i)&
	                    +(FPX(i)-FPX(i+1)&
	                    )*CellI&
	                    +(BXM(i))*TAU !Mhd Contribution
							  
	      YMOM2(i) = YMOM(i)&
	                    +(FPY(i)-FPY(i+1)&
	                    )*CellI&
	                    +(BYM(i))*TAU !Mhd Contribution
				  
	      ZMOM2(i) = ZMOM(i)&
	                    +(FPZ(i)-FPZ(i+1)&
	                    )*CellI&
	                    +(BZM(i))*TAU !Mhd Contribution

	      BIX2(i) = BIX(i)&      ! X-Magnetic Field Evolution
			    +((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    )*CellI+PSIBX(i)&
			    +BEVOX(i))*TAU
											 
	      BIY2(i) = BIY(i)&      ! Y-Magnetic Field Evolution
			    +((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    )*CellI+PSIBY(i)&
			    +BEVOY(i))*TAU
						  
	      BIZ2(i) = BIZ(i)&      ! Z-Magnetic Field Evolution
			    +((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    )*CellI+PSIBZ(i)&
			    +BEVOZ(i))*TAU

	      ENER2(i) = ENER(i)& 
	                    +(FE(i)-FE(i+1)&
	                    )*CellI&
	                    +(BEN(i))*TAU  !Mhd Contribution

!!#### end 1st order #######


	    End Do
!#### end RK2 1st step #######

   !---- HALL SUB-CYCLING
   If (HALLMHD) then
   SSIT = 0
   TIME_HALL = 0.D0
   MAX_THALL = TIME_HALL + TAU
   TAU_eps = 1.D-10
	      Do i=1-gc,IX+gc
		PSIH(i) = PSI(i)
		BIXH(i) = BIX(i)
		BIYH(i) = BIY(i)
		BIZH(i) = BIZ(i)
	      End Do
	Do 
	  If (TIME_HALL.gt.MAX_THALL) Exit
	    if((TIME_HALL+TAU_H).gt.(MAX_THALL)) then
	      TAU_H = (1.D0+TAU_eps)*MAX_THALL-TIME_HALL
	    end if
	  TIME_HALL = TIME_HALL + TAU_H
	  SSIT = SSIT + 1

	  Call Hall_MHD(Cell,gc,IX,DENS,XMOM,YMOM,ZMOM,ENER,&
	           BAX,BAY,BAZ,BIXH,BIYH,BIZH,PSIH,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
		   FBEVOX,FBEVOY,FBEVOZ,GBEVOX,GBEVOY,GBEVOZ,HBEVOX,HBEVOY,HBEVOZ,&
		   BPSI,PSIBX,PSIBY,PSIBZ,FPSI,GPSI,HPSI,&
	           Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	           temp, kappa,vmaxh,c_ratio,ch_mult)

	      Do i=1,IX
	      	PSITEMP(i) = PSIH(i)&
			    +((FPSI(i-1)-FPSI(i)& !MHD
			    )*CellI&
			   +BPSI(i))*TAU_H
	      	   PSI2(i) = PSI2(i)&
			    +((FPSI(i-1)-FPSI(i)& !MHD
			    )*CellI&
			   +BPSI(i))*TAU_H*0.5D0

	        BIXTEMP(i) = BIXH(i)&      ! X-Magnetic Field Evolution
			    +((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    )*CellI + PSIBX(i))*TAU_H
	           BIX2(i) = BIX2(i)&      ! X-Magnetic Field Evolution
			    +((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    )*CellI + PSIBX(i))*TAU_H*0.5D0
											 
	        BIYTEMP(i) = BIYH(i)&      ! Y-Magnetic Field Evolution
			    +((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    )*CellI + PSIBY(i))*TAU_H
	           BIY2(i) = BIY2(i)&      ! Y-Magnetic Field Evolution
			    +((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    )*CellI + PSIBY(i))*TAU_H*0.5D0
						  
	        BIZTEMP(i) = BIZH(i)&      ! Z-Magnetic Field Evolution
			    +((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    )*CellI + PSIBZ(i))*TAU_H
	           BIZ2(i) = BIZ2(i)&      ! Z-Magnetic Field Evolution
			    +((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    )*CellI + PSIBZ(i))*TAU_H*0.5D0
	      End Do
          Call BOUNDARY_HALL(IX, &
	                    Lchar,Cell,gc,&
	                    BIXTEMP,BIYTEMP,BIZTEMP,PSITEMP,B0)

	 !SECOND STEP
	  Call Hall_MHD(Cell,gc,IX,DENS,XMOM,YMOM,ZMOM,ENER,&
	           BAX,BAY,BAZ,BIXTEMP,BIYTEMP,BIZTEMP,PSITEMP,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
		   FBEVOX,FBEVOY,FBEVOZ,GBEVOX,GBEVOY,GBEVOZ,HBEVOX,HBEVOY,HBEVOZ,&
		   BPSI,PSIBX,PSIBY,PSIBZ,FPSI,GPSI,HPSI,&
	           Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	           temp, kappa,vmaxh,c_ratio,ch_mult)

	      Do i=1,IX
	      PSIH(i) = (PSITEMP(i)+PSIH(i)&
			    +((FPSI(i-1)-FPSI(i)& !MHD
			    )*CellI&
			   +BPSI(i))*TAU_H)*0.5D0
	      	   PSI2(i) = PSI2(i)&
			    +((FPSI(i-1)-FPSI(i)& !MHD
			    )*CellI&
			   +BPSI(i))*TAU_H*0.5D0

	        BIXH(i) = (BIXTEMP(i) + BIXH(i)&      ! X-Magnetic Field Evolution
			    +((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    )*CellI&
			    +PSIBX(i))*TAU_H&
			    )*0.5D0
	           BIX2(i) = BIX2(i)&      ! X-Magnetic Field Evolution
			    +((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    )*CellI + PSIBX(i))*TAU_H*0.5D0
											 
	        BIYH(i) = (BIYTEMP(i) + BIYH(i)&      ! Y-Magnetic Field Evolution
			    +((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    )*CellI&
			    +PSIBY(i))*TAU_H&
			    )*0.5D0
	           BIY2(i) = BIY2(i)&      ! Y-Magnetic Field Evolution
			    +((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    )*CellI + PSIBY(i))*TAU_H*0.5D0
						  
	        BIZH(i) = (BIZTEMP(i) + BIZH(i)&      ! Z-Magnetic Field Evolution
			    +((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    )*CellI&
			    +PSIBZ(i))*TAU_H&
			    )*0.5D0
	           BIZ2(i) = BIZ2(i)&      ! Z-Magnetic Field Evolution
			    +((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    )*CellI + PSIBZ(i))*TAU_H*0.5D0
	      End Do
          Call BOUNDARY_HALL(IX, &
	                    Lchar,Cell,gc,&
	                    BIXH,BIYH,BIZH,PSIH,B0)

	EndDo




   EndIf
   !----END HALL SUB-CYCLING ----
	Call BOUNDARY(CK,GAS_CONSTANT,DENSchar,TEMPchar,Lchar,gc,&
	              IX,DENS,XMOM2,&
	              YMOM2,ZMOM2,ENER2,temp,gradP,Cell,GAM,VISC,Uchar,&
	              BIX2,BIY2,BIZ2,PSI2,Bmu_0,COND,B0,&
	              periods)
!----- SECOND STEP FOR RK2 SCHEME 
   !---------------------
	Call MHD(Cell,gc,CK,IX,DENS,XMOM2,YMOM2,ZMOM2,ENER2,&
	         BAX,BAY,BAZ,BIX2,BIY2,BIZ2,PSI2,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
				    BPSI,PSIBX,PSIBY,PSIBZ,&
				    FPSI,FBEVOX,FBEVOY,FBEVOZ,&
				    GPSI,GBEVOX,GBEVOY,GBEVOZ,&
				    HPSI,HBEVOX,HBEVOY,HBEVOZ,&
	         Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	         temp, kappa,TAU,&
	         HALLMHD,vmax,c_ratio,ch_mult)

!!#### RK2 2nd step #######
	    Do i=1,IX

	      PSI(i) = 0.5D0*(PSI(i) + PSI2(i)&
			    +(FPSI(i-1)-FPSI(i)& !MHD
			    )*CellI*TAU&
			   +BPSI(i)*TAU)
						  
	      XMOM(i) = 0.5D0*(XMOM(i) + XMOM2(i)&
	                    +(FPX(i)-FPX(i+1)&
	                    )*CellI&
	                    +(BXM(i))*TAU) !Mhd Contribution
							  
	      YMOM(i) = 0.5D0*(YMOM(i) + YMOM2(i)&
	                    +(FPY(i)-FPY(i+1)&
	                    )*CellI&
	                    +(BYM(i))*TAU) !Mhd Contribution
				  
	      ZMOM(i) = 0.5D0*(ZMOM(i) + ZMOM2(i)&
	                    +(FPZ(i)-FPZ(i+1)&
	                    )*CellI&
	                    +(BZM(i))*TAU) !Mhd Contribution

	      BIX(i) = 0.5D0*(BIX(i) + BIX2(i)&      ! X-Magnetic Field Evolution
			    +((FBEVOX(i-1)-FBEVOX(i)& !MHD
			    )*CellI+PSIBX(i)&
			    +BEVOX(i))*TAU)
											 
	      BIY(i) = 0.5D0*(BIY(i) + BIY2(i)&      ! Y-Magnetic Field Evolution
			    +((FBEVOY(i-1)-FBEVOY(i)& !MHD
			    )*CellI+PSIBY(i)&
			    +BEVOY(i))*TAU)
						  
	      BIZ(i) = 0.5D0*(BIZ(i) + BIZ2(i)&      ! Z-Magnetic Field Evolution
			    +((FBEVOZ(i-1)-FBEVOZ(i)& !MHD
			    )*CellI+PSIBZ(i)&
			    +BEVOZ(i))*TAU)

	      ENER(i) = 0.5D0*(ENER(i) + ENER2(i)& 
	                    +(FE(i)-FE(i+1)&
	                    )*CellI&
	                    +(BEN(i))*TAU)  !Mhd Contribution

	      AE = (25.D-2)*(CK+3)*DENS(i)/(ENER(i)-(XMOM(i)*XMOM(i)&
	            +YMOM(i)*YMOM(i)+ZMOM(i)*ZMOM(i))/(2.D0*DENS(i)) )

	      temp1=1.D0/(2.D0*GAS_CONSTANT*AE)
              
	      If ((DENS(i)<0.0).or.(temp1<0.0).or.(DENS(i).ne.DENS(i))) Then
	        Write(*,*) 'Density or temperature negative',i,IT
	        write(*,'(4E12.3)') DENS(i),XMOM(i),ENER(i),temp1
	        !write(*,'(4E12.3)') PSI(i),BIX(i),BIY(i),BIZ(i)
	        !write(*,'(6E12.3)') FPSI(i-1),FPSI(i),BPSI(i),FBEVOX(i-1),FBEVOX(i),BEVOX(i)
	        !write(*,'(6E12.3)') FBEVOY(i-1),FBEVOY(i),BEVOY(i),FBEVOZ(i-1),FBEVOZ(i),BEVOZ(i)
	        !write(*,'(6E12.3)') PSIBX(i),PSIBY(i),PSIBZ(i),FPX(i),FPX(i+1),BXM(i)
	        !write(*,'(6E12.3)') FPY(i),FPY(i+1),BYM(i),FPZ(i),FPZ(i+1),BZM(i)
	        !write(*,'(5E12.3)') FM(i),FM(i+1),FE(i),FE(i+1),BEN(i)
	        write(*,'(6E12.3)') FM(i),FM(i+1),FPX(i),FPX(i+1),FE(i),FE(i+1)
	        Stop
	      End If
			  
	    End Do
		
	Return
	End Subroutine UPDATE


END MODULE update_sub
