This is a basic version of the MGKM code in 1-D

Currently the boundary and initial conditions are limited to simple shock-tube problems,
but this can be updated by modifying th mod_boundaries.f90 and mod_initialconditions.f90
files.

To compile, simply "make" using the included makefile.

Note: Since this 1D MGKM code is mostly a 'reduced' version of the fully parallelized
3-D MGKM code there may be some remaining variables which are not used or initialized,
so there may be several compiler warnings/errors.