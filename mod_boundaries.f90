! SHOCKTUBE BOUNDARY CONDITION
! ZERO-GRADIENT IN X
MODULE boundary_cond

USE function_module, ONLY : ch_cap, S_EQI
USE constants

IMPLICIT Double Precision (A-H,O-Z)


CONTAINS

!-----------------------------------------------------------------------------------------   
	Subroutine BOUNDARY(CK,GAS_CONSTANT,DENSchar,TEMPchar,Lchar,gc,&
	                    IX,DENS,XMOM,&
	                    YMOM,ZMOM,ENER,temp,gradP,Cell,GAM,VISC,&
			    Uchar,&
	                    BIX,BIY,BIZ,PSI,Bmu_0,COND,B0,&
			    periods)
			    
!-----------------------------------------------------------------------------------------
	Integer, Parameter :: id=9
	Integer :: gc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
	                            XMOM,YMOM,ZMOM,ENER,temp,BIX,BIY,BIZ,PSI
	Double Precision :: Lchar

	logical :: periods(0:2)

!------Boundary conditions (ghost cell values) for each processor set by sending
!------data to neighboring cells.

!........................................................................   
!				X BC
!........................................................................   
       
	    Do i=1,gc
	      DENS(IX+i)=DENS(IX+1-i)
	      XMOM(IX+i)=XMOM(IX+1-i)
	      YMOM(IX+i)=YMOM(IX+1-i)
	      ZMOM(IX+i)=ZMOM(IX+1-i)
	      ENER(IX+i)=ENER(IX+1-i)
	      BIX(IX+i)=BIX(IX+1-i)
	      BIY(IX+i)=BIY(IX+1-i)
	      BIZ(IX+i)=BIZ(IX+1-i)
	      PSI(IX+i)=PSI(IX+1-i)
	
	      DENS(1-i)=DENS(i)
	      XMOM(1-i)=XMOM(i)
	      YMOM(1-i)=YMOM(i)
	      ZMOM(1-i)=ZMOM(i)
	      ENER(1-i)=ENER(i)
	      BIX(1-i)=BIX(i)
	      BIY(1-i)=BIY(i)
	      BIZ(1-i)=BIZ(i)
	      PSI(1-i)=PSI(i)
	    End Do

	    Do i=1-gc,IX+gc
	      AE = (25.D-2)*(CK+3)*DENS(i)/(ENER(i)-(XMOM(i)*XMOM(i)&
	           +YMOM(i)*YMOM(i)+ZMOM(i)*ZMOM(i))/(2.D0*DENS(i)) )
	      temp(i)=1.D0/(2.D0*GAS_CONSTANT*AE)
	    End Do

	Return    
	End Subroutine BOUNDARY

	Subroutine BOUNDARY_HALL(IX, &
	                    Lchar,Cell,gc,&                    
	                    BIX,BIY,BIZ,PSI,B0)
!-----------------------------------------------------------------------------------------
	Integer, Parameter :: id=4
	Integer :: gc
	Double Precision, Dimension(1-gc:IX+gc) :: BIX, BIY, BIZ,PSI
	Double Precision :: Lchar

!------Boundary conditions (ghost cell values) for each processor set by sending
!------data to neighboring cells.
!........................................................................   
!				X BC
!........................................................................   
	    Do i=1,gc
	      BIX(IX+i)=BIX(IX+1-i)
	      BIY(IX+i)=BIY(IX+1-i)
	      BIZ(IX+i)=BIZ(IX+1-i)
	      PSI(IX+i)=PSI(IX+1-i)
	
	      BIX(1-i)=BIX(i)
	      BIY(1-i)=BIY(i)
	      BIZ(1-i)=BIZ(i)
	      PSI(1-i)=PSI(i)
	    End Do

	Return    
	End Subroutine BOUNDARY_HALL

END MODULE boundary_cond
