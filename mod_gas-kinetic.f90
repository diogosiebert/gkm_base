MODULE gkm_fluxes

USE WENO
USE function_module, ONLY : VISCF
USE constants

IMPLICIT Double Precision (A-H,O-Z)

CONTAINS

      Subroutine DXE(CK,U,V,W,AL,B1,B2,B3,B4,B5,X1,X2,X3,X4,X5)
      
      R5=2*B5-B1*(U*U+V*V+W*W+(CK+3)/(2*AL))
      R4=B4-W*B1
      R3=B3-V*B1
      R2=B2-U*B1
      X5=2*AL*AL*(R5-2*U*R2-2*V*R3-2*W*R4)/(CK+3)
      X4=2*(AL*R4-W*X5)
      X3=2*(AL*R3-V*X5)
      X2=2*(AL*R2-U*X5)
      X1=B1-X2*U-X3*V-X4*W-X5*(U*U+V*V+W*W+(CK+3)/(2*AL))
      Return
      End Subroutine DXE      
!-----------------------------------------------------------------------------

                     
      Subroutine DXEE(TAU,cell,gc,CK,GAS_CONSTANT,Pr,TEMP_MEAN,VISC,IX,&
                      temp,DENS,XMOM,YMOM,ZMOM,ENER,AFM,AFPX,&
                      AFPY,AFPZ,AFE)
      Integer :: gc
      Double Precision, Dimension(1-gc:IX+gc) :: DENS,XMOM,YMOM,ZMOM,ENER,&
                                  AFM,AFPX,AFPY,AFPZ,AFE,ADE1,AXM1,AYM1,AZM1,AEN1,AE1,ADE2,AXM2,&
                                  AYM2,AZM2,AEN2,AE2,DD,DX,DY,DZ,DE,temp,TEM,ATN1,ATN2
      Double Precision :: DERFC
      EP=1.D-15
      AA=1.D0
      CellI=1.D0/cell
      Rcell=1.D0/(2*cell)
      SPI=DSQRT(PI)

      Call weno_x(gc,IX,DENS,ADE1,ADE2)
      Call weno_x(gc,IX,XMOM,AXM1,AXM2)
      Call weno_x(gc,IX,YMOM,AYM1,AYM2)
      Call weno_x(gc,IX,ZMOM,AZM1,AZM2)
      Call weno_x(gc,IX,temp,ATN1,ATN2)


          Do i=1,IX+1
           i0=i-1
           AE1(i)=1.D0/(2*GAS_CONSTANT*ATN1(i))
           AE2(i)=1.D0/(2*GAS_CONSTANT*ATN2(i))
           
           AEN1(i) = (5.D-1)*(AXM1(i)*AXM1(i)+AYM1(i)*AYM1(i)+AZM1(i)*AZM1(i))/ADE1(i)&
						+(CK+3)*ADE1(i)/(4*AE1(i)) 

           AEN2(i) = (5.D-1)*(AXM2(i)*AXM2(i)+AYM2(i)*AYM2(i)+AZM2(i)*AZM2(i))/ADE2(i)&
						+(CK+3)*ADE2(i)/(4*AE2(i)) 

           if ((AE1(i)<0.0).or.(AE2(i)<0.0)) then
              write(*,*) i,j,k
              write(*,*) AE1(i),AE2(i)
              write(*,*) 'Negative temperature in DXEE'
              stop
           end if
          End Do

          Do i=1,IX+1
            i0=i-1
            RADE1=1.D0/ADE1(i)
            AXU1=AXM1(i)*RADE1
            AYU1=AYM1(i)*RADE1
            AZU1=AZM1(i)*RADE1
            RAE1=1.D0/AE1(i)

            RADE2=1.D0/ADE2(i)
            AXU2=AXM2(i)*RADE2
            AYU2=AYM2(i)*RADE2
            AZU2=AZM2(i)*RADE2
            RAE2=1.D0/AE2(i)
            
            B1=2*(ADE1(i)-DENS(i0))*CellI
            B2=2*(AXM1(i)-XMOM(i0))*CellI
            B3=2*(AYM1(i)-YMOM(i0))*CellI
            B4=2*(AZM1(i)-ZMOM(i0))*CellI
            B5=2*(AEN1(i)-ENER(i0))*CellI

            C1=2*(DENS(i)-ADE2(i))*CellI
            C2=2*(XMOM(i)-AXM2(i))*CellI
            C3=2*(YMOM(i)-AYM2(i))*CellI
            C4=2*(ZMOM(i)-AZM2(i))*CellI
            C5=2*(ENER(i)-AEN2(i))*CellI            

            Call DXE(CK,AXU1,AYU1,AZU1,AE1(i),B1,B2,B3,B4,B5,XL1,XL2,XL3,XL4,XL5)
            Call DXE(CK,AXU2,AYU2,AZU2,AE2(i),C1,C2,C3,C4,C5,XR1,XR2,XR3,XR4,XR5)

            B1= 0.D0
            B2= 0.D0
            B3= 0.D0
            B4= 0.D0
            B5= 0.D0

            C1= 0.D0
            C2= 0.D0
            C3= 0.D0
            C4= 0.D0
            C5= 0.D0

            Call DXE(CK,AXU1,AYU1,AZU1,AE1(i),B1,B2,B3,B4,B5,YL1,YL2,YL3,YL4,YL5)
            Call DXE(CK,AXU2,AYU2,AZU2,AE2(i),C1,C2,C3,C4,C5,YR1,YR2,YR3,YR4,YR5)

            B1= 0.D0
            B2= 0.D0
            B3= 0.D0
            B4= 0.D0
            B5= 0.D0

            C1= 0.D0
            C2= 0.D0
            C3= 0.D0
            C4= 0.D0
            C5= 0.D0

            Call DXE(CK,AXU1,AYU1,AZU1,AE1(i),B1,B2,B3,B4,B5,ZL1,ZL2,ZL3,ZL4,ZL5)
            Call DXE(CK,AXU2,AYU2,AZU2,AE2(i),C1,C2,C3,C4,C5,ZR1,ZR2,ZR3,ZR4,ZR5)
   

            SAE1=DSQRT(DABS(AE1(i)))
            SAE2=DSQRT(DABS(AE2(i)))
            
            TEU0=(5.D-1)*DERFC(-AXU1*SAE1)
            TEU1=AXU1*TEU0+(5.D-1)*DEXP(-AE1(i)*AXU1*AXU1)/(SAE1*SPI)
            TEU2=AXU1*TEU1+(5.D-1)*TEU0*RAE1
            TEU3=AXU1*TEU2+TEU1*RAE1
            TEU4=AXU1*TEU3+(15.D-1)*TEU2*RAE1
            TEU5=AXU1*TEU4+2*TEU3*RAE1
            TEU6=AXU1*TEU5+(25.D-1)*TEU4*RAE1
          
            TGU0=(5.D-1)*DERFC(AXU2*SAE2)
            TGU1=AXU2*TGU0-(5.D-1)*DEXP(-AE2(i)*AXU2*AXU2)/(SAE2*SPI)
            TGU2=AXU2*TGU1+(5.D-1)*TGU0*RAE2
            TGU3=AXU2*TGU2+TGU1*RAE2
            TGU4=AXU2*TGU3+(15.D-1)*TGU2*RAE2
            TGU5=AXU2*TGU4+2*TGU3*RAE2
            TGU6=AXU2*TGU5+(25.D-1)*TGU4*RAE2

            EX1=AXU1
            EX2=AXU1*EX1+(5.D-1)*RAE1
            EX3=AXU1*EX2+EX1*RAE1
            EX4=AXU1*EX3+(15.D-1)*EX2*RAE1
            EX5=AXU1*EX4+2.0*EX3*RAE1

            EY1=AYU1
            EY2=AYU1*EY1+(5.D-1)*RAE1
            EY3=AYU1*EY2+EY1*RAE1
            EY4=AYU1*EY3+(15.D-1)*EY2*RAE1
            EY5=AYU1*EY4+2.0*EY3*RAE1

            EZ1=AZU1
            EZ2=AZU1*EZ1+(5.D-1)*RAE1
            EZ3=AZU1*EZ2+EZ1*RAE1
            EZ4=AZU1*EZ3+(15.D-1)*EZ2*RAE1
            EZ5=AZU1*EZ4+2.0*EZ3*RAE1

            GX1=AXU2
            GX2=AXU2*GX1+(5.D-1)*RAE2
            GX3=AXU2*GX2+GX1*RAE2
            GX4=AXU2*GX3+(15.D-1)*GX2*RAE2
            GX5=AXU2*GX4+2.0*GX3*RAE2

            GY1=AYU2
            GY2=AYU2*GY1+(5.D-1)*RAE2
            GY3=AYU2*GY2+GY1*RAE2
            GY4=AYU2*GY3+(15.D-1)*GY2*RAE2
            GY5=AYU2*GY4+2*GY3*RAE2

            GZ1=AZU2
            GZ2=AZU2*GZ1+(5.D-1)*RAE2
            GZ3=AZU2*GZ2+GZ1*RAE2
            GZ4=AZU2*GZ3+(15.D-1)*GZ2*RAE2
            GZ5=AZU2*GZ4+2*GZ3*RAE2
           
            EI2=(5.D-1)*CK*RAE1
            EI4=(25.D-2)*CK*(CK+2)*RAE1*RAE1

            GI2=(5.D-1)*CK*RAE2
            GI4=(25.D-2)*CK*(CK+2)*RAE2*RAE2
            
            EXU=EY2+EZ2+EI2
            EYU=EY3+EY1*(EZ2+EI2)
            EZU=(EY2+EI2)*EZ1+EZ3
            SN1=(EX2*EY1+EY3)*EZ1+EY1*(EZ3+EZ1*EI2)



            GXU=GY2+GZ2+GI2
            GYU=GY3+GY1*(GZ2+GI2)
            GZU=(GY2+GI2)*GZ1+GZ3
            SN2=(GX2*GY1+GY3)*GZ1+GY1*(GZ3+GZ1*GI2)
 
           RX1=XL1*(EX3+EX1*EXU)&
              +XL2*(EX4+EX2*EXU)&
              +XL3*(EX3*EY1+EX1*EYU)&
              +XL4*(EX3*EZ1+EX1*EZU)&
              +XL5*(EX5+EX1*(EY4+EZ4+EI4)+2*EX3*EXU&
                 +2*EX1*EY2*(EZ2+EI2)+2*EX1*EZ2*EI2)

           RY1=YL1*(EX2*EY1+EYU)&
              +YL2*(EX3*EY1+EX1*EYU)&
              +YL3*(EY2*(EX2+EZ2+EI2)+EY4)&
              +YL4*SN1+YL5*((EX4+EZ4+EI4)*EY1+EY5&
                +2*EX2*EYU+2*EY3*(EZ2+EI2)+2*EY1*EZ2*EI2)
     
           RZ1=ZL1*(EX2*EZ1+EZU)&
              +ZL2*(EX3*EZ1+EX1*EZU)&
              +ZL3*SN1+ZL4*(EZ2*(EX2+EY2+EI2)+EZ4)&
              +ZL5*(EZ1*(EX4+EY4+EI4)+EZ5&

                  +2*EX2*EZU+2*EY2*(EZ3+EZ1*EI2)+2*EZ3*EI2)

           RX2=XR1*(GX3+GX1*GXU)&
              +XR2*(GX4+GX2*GXU)&
              +XR3*(GX3*GY1+GX1*GYU)&
              +XR4*(GX3*GZ1+GX1*GZU)&
              +XR5*(GX5+GX1*(GY4+GZ4+GI4)+2*GX3*GXU&
                 +2*GX1*GY2*(GZ2+GI2)+2*GX1*GZ2*GI2)

           RY2=YR1*(GX2*GY1+GYU)&
              +YR2*(GX3*GY1+GX1*GYU)&
              +YR3*(GX2*GY2+GY4+GY2*(GZ2+GI2))&
              +YR4*SN2+YR5*((GX4+GZ4+GI4)*GY1+GY5&
                +2*GX2*GYU+2*GY3*(GZ2+GI2)+2*GY1*GZ2*GI2)
     

           RZ2=ZR1*(GX2*GZ1+GZU)&
              +ZR2*(GX3*GZ1+GX1*GZU)&
              +ZR3*SN2+ZR4*(GX2*GZ2+(GY2+GI2)*GZ2+GZ4)&
              +ZR5*(GZ1*(GX4+GY4+GI4)+GZ5&
                  +2*GX2*GZU+2*GY2*(GZ3+GZ1*GI2)+2*GZ3*GI2)

! Involving a_L and a_R in the calculation of A_L and A_R: DONE

           AL=-(XL1*EX1+XL2*EX2+EX1*(XL3*EY1+XL4*EZ1)&
               +XL5*(EX3+EX1*EXU)&
              +(YL1+YL2*EX1)*EY1+YL3*EY2+YL4*EY1*EZ1&
               +YL5*((EX2+EZ2+EI2)*EY1+EY3)&
              +(ZL1+ZL2*EX1)*EZ1+ZL3*EY1*EZ1+ZL4*EZ2&
               +ZL5*((EX2+EY2+EI2)*EZ1+EZ3))
     
           BL=-(XL1*EX2+XL2*EX3+EX2*(XL3*EY1+XL4*EZ1)&
               +XL5*(EX4+EX2*EXU)&
              +(YL1*EX1+YL2*EX2)*EY1+EX1*(YL3*EY2+YL4*EY1*EZ1)&
               +YL5*((EX3+EX1*(EZ2+EI2))*EY1+EX1*EY3)&
              +(ZL1*EX1+ZL2*EX2)*EZ1+EX1*(ZL3*EY1*EZ1+ZL4*EZ2)&
               +ZL5*((EX3+EX1*(EY2+EI2))*EZ1+EX1*EZ3))
     
           CL=-((XL1*EX1+XL2*EX2)*EY1+EX1*(XL3*EY2+XL4*EY1*EZ1)&
                +XL5*(EX3*EY1+EX1*EYU)&
               +(YL1+YL2*EX1)*EY2+YL3*EY3+YL4*EY2*EZ1&
                +YL5*((EX2+EZ2+EI2)*EY2+EY4)&
               +(ZL1+ZL2*EX1)*EY1*EZ1+ZL3*EY2*EZ1+ZL4*EY1*EZ2&
                +ZL5*SN1)
      
           DL=-((XL1*EX1+XL2*EX2)*EZ1+EX1*(XL3*EY1*EZ1+XL4*EZ2)&
                   +XL5*(EX3*EZ1+EX1*EZU)&
               +(YL1+YL2*EX1)*EY1*EZ1+YL3*EY2*EZ1+YL4*EY1*EZ2&
                 +YL5*SN1+(ZL1+ZL2*EX1)*EZ2+ZL3*EY1*EZ2+ZL4*EZ3&
               +ZL5*((EX2+EY2+EI2)*EZ2+EZ4))

           AR=-(XR1*GX1+XR2*GX2+GX1*(XR3*GY1+XR4*GZ1)&
               +XR5*(GX3+GX1*GXU)&
              +(YR1+YR2*GX1)*GY1+YR3*GY2+YR4*GY1*GZ1&
               +YR5*((GX2+GZ2+GI2)*GY1+GY3)&
              +(ZR1+ZR2*GX1)*GZ1+ZR3*GY1*GZ1+ZR4*GZ2&
               +ZR5*((GX2+GY2+GI2)*GZ1+GZ3))
     
           BR=-(XR1*GX2+XR2*GX3+GX2*(XR3*GY1+XR4*GZ1)&
               +XR5*(GX4+GX2*GXU)&
              +(YR1*GX1+YR2*GX2)*GY1+GX1*(YR3*GY2+YR4*GY1*GZ1)&
               +YR5*((GX3+GX1*(GZ2+GI2))*GY1+GX1*GY3)&
              +(ZR1*GX1+ZR2*GX2)*GZ1+GX1*(ZR3*GY1*GZ1+ZR4*GZ2)&
               +ZR5*((GX3+GX1*(GY2+GI2))*GZ1+GX1*GZ3))
     
           CR=-((XR1*GX1+XR2*GX2)*GY1+GX1*(XR3*GY2+XR4*GY1*GZ1)&
                +XR5*(GX3*GY1+GX1*GYU)&
               +(YR1+YR2*GX1)*GY2+YR3*GY3+YR4*GY2*GZ1&
                +YR5*((GX2+GZ2+GI2)*GY2+GY4)&
               +(ZR1+ZR2*GX1)*GY1*GZ1+ZR3*GY2*GZ1+ZR4*GY1*GZ2&
                +ZR5*SN2)
      
           DR=-((XR1*GX1+XR2*GX2)*GZ1+GX1*(XR3*GY1*GZ1+XR4*GZ2)&
                +XR5*(GX3*GZ1+GX1*GZU)&
               +(YR1+YR2*GX1)*GY1*GZ1+YR3*GY2*GZ1+YR4*GY1*GZ2&
                +YR5*SN2+(ZR1+ZR2*GX1)*GZ2+ZR3*GY1*GZ2+ZR4*GZ3&
                +ZR5*((GX2+GY2+GI2)*GZ2+GZ4))
     
           EL=-(5.D-1)*(RX1+RY1+RZ1)
           ER=-(5.D-1)*(RX2+RY2+RZ2)
      
           Call DXE(CK,AXU1,AYU1,AZU1,AE1(i),AL,BL,CL,DL,EL,A1,B1,C1,D1,E1)
           Call DXE(CK,AXU2,AYU2,AZU2,AE2(i),AR,BR,CR,DR,ER,A2,B2,C2,D2,E2)
      

           ADE=ADE1(i)*TEU0+ADE2(i)*TGU0
           AXM=ADE1(i)*TEU1+ADE2(i)*TGU1
           AYM=ADE1(i)*TEU0*EY1+ADE2(i)*TGU0*GY1
           AZM=ADE1(i)*TEU0*EZ1+ADE2(i)*TGU0*GZ1
           AEN=(5.D-1)*(ADE1(i)*(TEU2+TEU0*EXU)&
                       +ADE2(i)*(TGU2+TGU0*GXU))		   
           RADE=1.D0/ADE
           AE0=(25.D-2)*(CK+3)*ADE/(AEN-(5.D-1)*(AXM*AXM+AYM*AYM+AZM*AZM)*RADE)
           AXU0=AXM*RADE
           AYU0=AYM*RADE
           AZU0=AZM*RADE
           RAE0=1.D0/AE0
           temp1=1.D0/(2*GAS_CONSTANT*AE0)
          
           AA1=ADE1(i)*RAE1
           AA2=ADE2(i)*RAE2

           TE=2*AE0*VISCF(VISC,TEMP_MEAN,temp1)*RADE+TAU*DABS((AA1-AA2)/(AA1+AA2))
      
           SP=-TAU/TE
           SW=DEXP(SP)
           SE1=TE*(1.D0-SW)
           SET0=TE*(-TAU+SE1)
!          RSET0=1.D0/SET0
           SET1=-TE*TAU*SW+TE*SE1
           SET2=SET0+SET1

           SETV=SET1+SE1*TE
           SETA=SE1*TE


           CM0 = YL1*TEU0+YL2*TEU1+TEU0*(YL3*EY1+YL4*EZ1)&
                +YL5*(TEU2+TEU0*EXU)&
                +YR1*TGU0+YR2*TGU1+TGU0*(YR3*GY1+YR4*GZ1)&
                +YR5*(TGU2+TGU0*GXU)

           CX0 = YL1*TEU1+YL2*TEU2+TEU1*(YL3*EY1+YL4*EZ1)&
                +YL5*(TEU3+TEU1*EXU)&
                +YR1*TGU1+YR2*TGU2+TGU1*(YR3*GY1+YR4*GZ1)&
                +YR5*(TGU3+TGU1*GXU)

           CY0 = EY1*(YL1*TEU0+YL2*TEU1)+TEU0*(YL3*EY2+YL4*EY1*EZ1)&

                +YL5*(TEU2*EY1+TEU0*EYU)&
                +GY1*(YR1*TGU0+YR2*TGU1)+TGU0*(YR3*GY2+YR4*GY1*GZ1)&
                +YR5*(TGU2*GY1+TGU0*GYU)

           CZ0 = EZ1*(YL1*TEU0+YL2*TEU1)+TEU0*(YL3*EY1*EZ1+YL4*EZ2)&
                +YL5*(TEU2*EZ1+TEU0*EZU)&
                +GZ1*(YR1*TGU0+YR2*TGU1)+TGU0*(YR3*GY1*GZ1+YR4*GZ2)&
                +YR5*(TGU2*GZ1+TGU0*GZU)

           acu=TEU4+TEU0*(EY4+EZ4+EI4)+2*TEU2*EXU+2*TEU0*EY2*(EZ2+EI2)+2*TEU0*EZ2*EI2
           acg=TGU4+TGU0*(GY4+GZ4+GI4)+2*TGU2*GXU+2*TGU0*GY2*(GZ2+GI2)+2*TGU0*GZ2*GI2

           CE0 = (5.D-1)*(YL1*(TEU2+TEU0*EXU)&
                         +YL2*(TEU3+TEU1*EXU)&
                         +YL3*(TEU2*EY1+TEU0*EYU)&
                         +YL4*(TEU2*EZ1+TEU0*EZU)&
                         +YL5*acu)&
                +(5.D-1)*(YR1*(TGU2+TGU0*GXU)&
                         +YR2*(TGU3+TGU1*GXU)&
                         +YR3*(TGU2*GY1+TGU0*GYU)&
                         +YR4*(TGU2*GZ1+TGU0*GZU)&
                         +YR5*acg)                            

           Call DXE(CK,AXU0,AYU0,AZU0,AE0,CM0,CX0,CY0,CZ0,CE0,XB1,XB2,XB3,XB4,XB5)

           CM0 = ZL1*TEU0+ZL2*TEU1+TEU0*(ZL3*EY1+ZL4*EZ1)&
                +ZL5*(TEU2+TEU0*EXU)&
                +ZR1*TGU0+ZR2*TGU1+TGU0*(ZR3*GY1+ZR4*GZ1)&
                +ZR5*(TGU2+TGU0*GXU)

           CX0 = ZL1*TEU1+ZL2*TEU2+TEU1*(ZL3*EY1+ZL4*EZ1)&
                +ZL5*(TEU3+TEU1*EXU)&
                +ZR1*TGU1+ZR2*TGU2+TGU1*(ZR3*GY1+ZR4*GZ1)&
                +ZR5*(TGU3+TGU1*GXU)

           CY0 = EY1*(ZL1*TEU0+ZL2*TEU1)+TEU0*(ZL3*EY2+ZL4*EY1*EZ1)&
                +ZL5*(TEU2*EY1+TEU0*EYU)&
                +GY1*(ZR1*TGU0+ZR2*TGU1)+TGU0*(ZR3*GY2+ZR4*GY1*GZ1)&
                +ZR5*(TGU2*GY1+TGU0*GYU)

           CZ0 = EZ1*(ZL1*TEU0+ZL2*TEU1)+TEU0*(ZL3*EY1*EZ1+ZL4*EZ2)&
                +ZL5*(TEU2*EZ1+TEU0*EZU)&
                +GZ1*(ZR1*TGU0+ZR2*TGU1)+TGU0*(ZR3*GY1*GZ1+ZR4*GZ2)&
                +ZR5*(TGU2*GZ1+TGU0*GZU)

           CE0 = (5.D-1)*(ZL1*(TEU2+TEU0*EXU)&

                         +ZL2*(TEU3+TEU1*EXU)&
                         +ZL3*(TEU2*EY1+TEU0*EYU)&
                         +ZL4*(TEU2*EZ1+TEU0*EZU)&
                         +ZL5*acu)&
                +(5.D-1)*(ZR1*(TGU2+TGU0*GXU)&
                         +ZR2*(TGU3+TGU1*GXU)&
                         +ZR3*(TGU2*GY1+TGU0*GYU)&
                         +ZR4*(TGU2*GZ1+TGU0*GZU)&
                         +ZR5*acg)                            

           Call DXE(CK,AXU0,AYU0,AZU0,AE0,CM0,CX0,CY0,CZ0,CE0,XC1,XC2,XC3,XC4,XC5)
!...........................................................................................           
          
!  Inolving A_L and A_R in the calculation of \bar(A): DONE

           AMI = A1*TEU0+B1*TEU1+TEU0*(C1*EY1+D1*EZ1)+E1*(TEU2+TEU0*EXU)&
                +A2*TGU0+B2*TGU1+TGU0*(C2*GY1+D2*GZ1)+E2*(TGU2+TGU0*GXU)

           APIX = A1*TEU1+B1*TEU2+TEU1*(C1*EY1+D1*EZ1)+E1*(TEU3+TEU1*EXU)&
                 +A2*TGU1+B2*TGU2+TGU1*(C2*GY1+D2*GZ1)+E2*(TGU3+TGU1*GXU)

           APIY = EY1*(A1*TEU0+B1*TEU1)+TEU0*(C1*EY2+D1*EY1*EZ1)&
                      +E1*(TEU2*EY1+TEU0*EYU)&
                 +GY1*(A2*TGU0+B2*TGU1)+TGU0*(C2*GY2+D2*GY1*GZ1)&
                      +E2*(TGU2*GY1+TGU0*GYU)

           APIZ = EZ1*(A1*TEU0+B1*TEU1)+TEU0*(C1*EY1*EZ1+D1*EZ2)&
                      +E1*(TEU2*EZ1+TEU0*EZU)&
                 +GZ1*(A2*TGU0+B2*TGU1)+TGU0*(C2*GY1*GZ1+D2*GZ2)&
                      +E2*(TGU2*GZ1+TGU0*GZU)

           AEI = (5.D-1)*(A1*(TEU2+TEU0*EXU)&
                         +B1*(TEU3+TEU1*EXU)&
                         +C1*(TEU2*EY1+TEU0*EYU)&
                         +D1*(TEU2*EZ1+TEU0*EZU)&
                         +E1*acu)&

                +(5.D-1)*(A2*(TGU2+TGU0*GXU)&
                         +B2*(TGU3+TGU1*GXU)&
                         +C2*(TGU2*GY1+TGU0*GYU)&
                         +D2*(TGU2*GZ1+TGU0*GZU)&
                         +E2*acg)      
!..................................................................................................
      
!  FLUXES, INVOLVING a_L, a_R: DONE

           EW1 = -SETV*(XL1*TEU2+XL2*TEU3+TEU2*(XL3*EY1+XL4*EZ1)&
                      +XL5*(TEU4+TEU2*EXU)&
                     +(YL1*TEU1+YL2*TEU2)*EY1+TEU1*(YL3*EY2+YL4*EY1*EZ1)&
                      +YL5*(TEU3*EY1+TEU1*EYU)&
                     +(ZL1*TEU1+ZL2*TEU2)*EZ1+TEU1*(ZL3*EY1*EZ1+ZL4*EZ2)&
                      +ZL5*(TEU3*EZ1+TEU1*EZU))

           FM1 = SE1*ADE1(i)*TEU1 + EW1
      
           FPX1 = SE1*ADE1(i)*TEU2&
                 -SETV*(XL1*TEU3+XL2*TEU4+TEU3*(XL3*EY1+XL4*EZ1)&
                       +XL5*(TEU5+TEU3*EXU)&
                      +(YL1*TEU2+YL2*TEU3)*EY1+TEU2*(YL3*EY2+YL4*EY1*EZ1)&
                       +YL5*(TEU4*EY1+TEU2*EYU)&
                      +(ZL1*TEU2+ZL2*TEU3)*EZ1+TEU2*(ZL3*EY1*EZ1+ZL4*EZ2)&
                       +ZL5*(TEU4*EZ1+TEU2*EZU))
      
           FPY1 = SE1*ADE1(i)*TEU1*EY1&
                 -SETV*((XL1*TEU2+XL2*TEU3)*EY1+TEU2*(XL3*EY2+XL4*EY1*EZ1)&
                        +XL5*(TEU4*EY1+TEU2*EYU)&
                       +(YL1*TEU1+YL2*TEU2)*EY2+TEU1*(YL3*EY3+YL4*EY2*EZ1)&
                        +YL5*(TEU3*EY2+TEU1*(EY4+EY2*(EZ2+EI2)))&
                       +(ZL1*TEU1+ZL2*TEU2)*EY1*EZ1+TEU1*(ZL3*EY2*EZ1+ZL4*EY1*EZ2)&
                        +ZL5*(TEU3*EY1*EZ1+TEU1*(EY3*EZ1+EY1*(EZ3+EZ1*EI2))))
      
           FPZ1 = SE1*ADE1(i)*TEU1*EZ1&
                 -SETV*((XL1*TEU2+XL2*TEU3)*EZ1+TEU2*(XL3*EY1*EZ1+XL4*EZ2)&
                        +XL5*(TEU4*EZ1+TEU2*EZU)&
                       +(YL1*TEU1+YL2*TEU2)*EY1*EZ1+TEU1*(YL3*EY2*EZ1+YL4*EY1*EZ2)&
                        +YL5*(TEU3*EY1*EZ1+TEU1*(EZ1*(EY3+EY1*EI2)+EY1*EZ3))&
                       +(ZL1*TEU1+ZL2*TEU2)*EZ2+TEU1*(ZL3*EY1*EZ2+ZL4*EZ3)&
                        +ZL5*(TEU3*EZ2+TEU1*(EZ2*(EY2+EI2)+EZ4)))       

           CEX = XL1*(TEU4+TEU2*EXU)&
                +XL2*(TEU5+TEU3*EXU)&
                +XL3*(TEU4*EY1+TEU2*EYU)&
                +XL4*(TEU4*EZ1+TEU2*EZU)&
                +XL5*(TEU6+TEU2*(EY4+EZ4+EI4)&
                +2*TEU4*EXU+2*TEU2*EY2*(EZ2+EI2)+2*TEU2*EZ2*EI2)

           CEY = YL1*(TEU3*EY1+TEU1*EYU)&
                +YL2*(TEU4*EY1+TEU2*EYU)&
                +YL3*(TEU3*EY2+TEU1*(EY4+EY2*(EZ2+EI2)))&
                +YL4*(TEU3*EY1*EZ1+TEU1*(EY3*EZ1+EY1*(EZ3+EZ1*EI2)))&
                +YL5*(TEU5*EY1+TEU1*(EY5+EY1*(EZ4+EI4))&
                +2*TEU3*EYU+2*TEU1*EY3*(EZ2+EI2)+2*TEU1*EY1*EZ2*EI2)

           CEZ = ZL1*(TEU3*EZ1+TEU1*EZU)&
                +ZL2*(TEU4*EZ1+TEU2*EZU)&
                +ZL3*(TEU3*EY1*EZ1+TEU1*(EY3*EZ1+EY1*(EZ3+EZ1*EI2)))&
                +ZL4*(TEU3*EZ2+TEU1*(EZ2*(EY2+EI2)+EZ4))&
                +ZL5*(TEU5*EZ1+TEU1*(EZ1*(EY4+EI4)+EZ5)&
                +2*TEU3*EZU+2*TEU1*EY2*(EZ3+EZ1*EI2)+2*TEU1*EZ3*EI2)
          
           FE1 = (5.D-1)*SE1*ADE1(i)*(TEU3+TEU1*EXU)&
                -(5.D-1)*SETV*(CEX+CEY+CEZ)
    
           EW2 = -SETV*(XR1*TGU2+XR2*TGU3+TGU2*(XR3*GY1+XR4*GZ1)&
                      +XR5*(TGU4+TGU2*GXU)&
                     +(YR1*TGU1+YR2*TGU2)*GY1+TGU1*(YR3*GY2+YR4*GY1*GZ1)&
                      +YR5*(TGU3*GY1+TGU1*GYU)&
                     +(ZR1*TGU1+ZR2*TGU2)*GZ1+TGU1*(ZR3*GY1*GZ1+ZR4*GZ2)&
                      +ZR5*(TGU3*GZ1+TGU1*GZU))

           FM2 = SE1*ADE2(i)*TGU1 + EW2
      
           FPX2 = SE1*ADE2(i)*TGU2&
                 -SETV*(XR1*TGU3+XR2*TGU4+TGU3*(XR3*GY1+XR4*GZ1)&
                       +XR5*(TGU5+TGU3*GXU)&
                      +(YR1*TGU2+YR2*TGU3)*GY1+TGU2*(YR3*GY2+YR4*GY1*GZ1)&
                       +YR5*(TGU4*GY1+TGU2*GYU)&
                      +(ZR1*TGU2+ZR2*TGU3)*GZ1+TGU2*(ZR3*GY1*GZ1+ZR4*GZ2)&
                       +ZR5*(TGU4*GZ1+TGU2*GZU))
      
           FPY2 = SE1*ADE2(i)*TGU1*GY1&
                 -SETV*((XR1*TGU2+XR2*TGU3)*GY1+TGU2*(XR3*GY2+XR4*GY1*GZ1)&
                        +XR5*(TGU4*GY1+TGU2*GYU)&
                       +(YR1*TGU1+YR2*TGU2)*GY2+TGU1*(YR3*GY3+YR4*GY2*GZ1)&
                        +YR5*(TGU3*GY2+TGU1*(GY4+GY2*(GZ2+GI2)))&
                       +(ZR1*TGU1+ZR2*TGU2)*GY1*GZ1+TGU1*(ZR3*GY2*GZ1+ZR4*GY1*GZ2)&
                        +ZR5*(TGU3*GY1*GZ1+TGU1*(GY3*GZ1+GY1*(GZ3+GZ1*GI2))))
     
           FPZ2 = SE1*ADE2(i)*TGU1*GZ1&
                 -SETV*((XR1*TGU2+XR2*TGU3)*GZ1+TGU2*(XR3*GY1*GZ1+XR4*GZ2)&
                        +XR5*(TGU4*GZ1+TGU2*GZU)&
                       +(YR1*TGU1+YR2*TGU2)*GY1*GZ1+TGU1*(YR3*GY2*GZ1+YR4*GY1*GZ2)&
                        +YR5*(TGU3*GY1*GZ1+TGU1*(GZ1*(GY3+GY1*GI2)+GY1*GZ3))&
                       +(ZR1*TGU1+ZR2*TGU2)*GZ2+TGU1*(ZR3*GY1*GZ2+ZR4*GZ3)&
                        +ZR5*(TGU3*GZ2+TGU1*(GZ2*(GY2+GI2)+GZ4)))

           CEX = XR1*(TGU4+TGU2*GXU)&
                +XR2*(TGU5+TGU3*GXU)&
                +XR3*(TGU4*GY1+TGU2*GYU)&
                +XR4*(TGU4*GZ1+TGU2*GZU)&
                +XR5*(TGU6+TGU2*(GY4+GZ4+GI4)&
                +2*TGU4*GXU+2*TGU2*GY2*(GZ2+GI2)+2*TGU2*GZ2*GI2)

           CEY = YR1*(TGU3*GY1+TGU1*GYU)&
                +YR2*(TGU4*GY1+TGU2*GYU)&
                +YR3*(TGU3*GY2+TGU1*(GY4+GY2*(GZ2+GI2)))&
                +YR4*(TGU3*GY1*GZ1+TGU1*(GY3*GZ1+GY1*(GZ3+GZ1*GI2)))&
                +YR5*(TGU5*GY1+TGU1*(GY5+GY1*(GZ4+GI4))&
                +2*TGU3*GYU+2*TGU1*GY3*(GZ2+GI2)+2*TGU1*GY1*GZ2*GI2)

           CEZ = ZR1*(TGU3*GZ1+TGU1*GZU)&
                +ZR2*(TGU4*GZ1+TGU2*GZU)&
                +ZR3*(TGU3*GY1*GZ1+TGU1*(GY3*GZ1+GY1*(GZ3+GZ1*GI2)))&
                +ZR4*(TGU3*GZ2+TGU1*(GZ2*(GY2+GI2)+GZ4))&
                +ZR5*(TGU5*GZ1+TGU1*(GZ1*(GY4+GI4)+GZ5)&
                +2*TGU3*GZU+2*TGU1*GY2*(GZ3+GZ1*EI2)+2*TGU1*GZ3*GI2)
           
           FE2 = (5.D-1)*SE1*ADE2(i)*(TGU3+TGU1*GXU)&
                -(5.D-1)*SETV*(CEX+CEY+CEZ)

!  FLUXES, INVOLVING A_L, A_R: DONE

           FMA = APIX
      
           FPXA = A1*TEU2+B1*TEU3+TEU2*(C1*EY1+D1*EZ1)+E1*(TEU4+TEU2*EXU)&
                 +A2*TGU2+B2*TGU3+TGU2*(C2*GY1+D2*GZ1)+E2*(TGU4+TGU2*GXU)
      
           FPYA = EY1*(A1*TEU1+B1*TEU2)+TEU1*(C1*EY2+D1*EY1*EZ1)&
                      +E1*(TEU3*EY1+TEU1*EYU)&
                 +GY1*(A2*TGU1+B2*TGU2)+TGU1*(C2*GY2+D2*GY1*GZ1)&
                      +E2*(TGU3*GY1+TGU1*GYU)
                      
           FPZA = EZ1*(A1*TEU1+B1*TEU2)+TEU1*(C1*EY1*EZ1+D1*EZ2)&
                      +E1*(TEU3*EZ1+TEU1*EZU)&
                 +GZ1*(A2*TGU1+B2*TGU2)+TGU1*(C2*GY1*GZ1+D2*GZ2)&
                      +E2*(TGU3*GZ1+TGU1*GZU)

      
           FEA = (5.D-1)*(A1*(TEU3+TEU1*EXU)&
                         +B1*(TEU4+TEU2*EXU)&
                         +C1*(TEU3*EY1+TEU1*EYU)&
                         +D1*(TEU3*EZ1+TEU1*EZU)&
                         +E1*(TEU5+TEU1*(EY4+EZ4+EI4)&
                         +2*TEU3*EXU+2*TEU1*EY2*(EZ2+EI2)+2*TEU1*EZ2*EI2))&
                +(5.D-1)*(A2*(TGU3+TGU1*GXU)&
                         +B2*(TGU4+TGU2*GXU)&
                         +C2*(TGU3*GY1+TGU1*GYU)&
                         +D2*(TGU3*GZ1+TGU1*GZU)&
                         +E2*(TGU5+TGU1*(GY4+GZ4+GI4)&
                         +2*TGU3*GXU+2*TGU1*GY2*(GZ2+GI2)+2*TGU1*GZ2*GI2))      
!................................................................................................

! Calculation of \bar(a)_L and \bar(a)_R

           AWL  = 2*(ADE-DENS(i0))*CellI
           BXWL = 2*(AXM-XMOM(i0))*CellI
           BYWL = 2*(AYM-YMOM(i0))*CellI

           BZWL = 2*(AZM-ZMOM(i0))*CellI
           CWL  = 2*(AEN-ENER(i0))*CellI

           AWR  = 2*(DENS(i)-ADE)*CellI
           BXWR = 2*(XMOM(i)-AXM)*CellI
           BYWR = 2*(YMOM(i)-AYM)*CellI
           BZWR = 2*(ZMOM(i)-AZM)*CellI
           CWR  = 2*(ENER(i)-AEN)*CellI

           Call DXE(CK,AXU0,AYU0,AZU0,AE0,AWL,BXWL,BYWL,BZWL,CWL,XL,YL,YXL,ZXL,ZL)
           Call DXE(CK,AXU0,AYU0,AZU0,AE0,AWR,BXWR,BYWR,BZWR,CWR,XR,YR,YXR,ZXR,ZR)
      
          
           SEU0=(5.D-1)*DERFC(-AXU0*DSQRT(AE0))
           SEU1=AXU0*SEU0+(5.D-1)*DEXP(-AE0*AXU0*AXU0)/DSQRT(AE0*PI)
           SEU2=AXU0*SEU1+(5.D-1)*SEU0*RAE0
           SEU3=AXU0*SEU2+SEU1*RAE0
           SEU4=AXU0*SEU3+(15.D-1)*SEU2*RAE0
           SEU5=AXU0*SEU4+2*SEU3*RAE0
           SEU6=AXU0*SEU5+(25.D-1)*SEU4*RAE0

           S1=AXU0
           S2=AXU0*S1+(5.D-1)*RAE0
           S3=AXU0*S2+S1*RAE0
           S4=AXU0*S3+(15.D-1)*S2*RAE0
           S5=AXU0*S4+2*S3*RAE0
           S6=AXU0*S5+(25.D-1)*S4*RAE0

!          SGU0=1.D0-SEU0
!          SGU1=S1-SEU1
           SGU2=S2-SEU2
           SGU3=S3-SEU3
           SGU4=S4-SEU4
           SGU5=S5-SEU5
           SGU6=S6-SEU6
           
           Y1=AYU0
           Y2=AYU0*Y1+(5.D-1)*RAE0
           Y3=AYU0*Y2+Y1*RAE0
           Y4=AYU0*Y3+(15.D-1)*Y2*RAE0
           Y5=AYU0*Y4+2*Y3*RAE0

           Z1=AZU0
           Z2=AZU0*Z1+(5.D-1)*RAE0
           Z3=AZU0*Z2+Z1*RAE0
           Z4=AZU0*Z3+(15.D-1)*Z2*RAE0
           Z5=AZU0*Z4+2*Z3*RAE0

           SI2=(5.D-1)*CK*RAE0
           SI4=(25.D-2)*CK*(CK+2)*RAE0*RAE0
        

! Terms involving \bar(a)_L and \bar(a)_R needed for the calculations of the fluxes

           SXU = Y2+Z2+SI2
           SYU = Y3+Y1*(Z2+SI2)
           SZU = Z1*(Y2+SI2)+Z3



           PY1 = Y1*(XB1*S2+XB2*S3)+S2*(XB3*Y2+XB4*Y1*Z1)&
                    +XB5*(Y1*(S4+S2*(Z2+SI2))+S2*Y3)

           PZ1 = Z1*(XC1*S2+XC2*S3)+S2*(XC3*Y1*Z1+XC4*Z2)&
                    +XC5*(Z1*(S4+S2*(Y2+SI2))+S2*Z3)

           PY2 = Y2*(XB1*S1+XB2*S2)+S1*(XB3*Y3+XB4*Y2*Z1)&
                    +XB5*(Y2*(S3+S1*(Z2+SI2))+S1*Y4)

           PZ2 = Y1*Z1*(XC1*S1+XC2*S2)+S1*(XC3*Y2*Z1+XC4*Y1*Z2)&
                       +XC5*(Y1*Z1*(S3+S1*SI2)+S1*(Y3*Z1+Y1*Z3))

           PY3 = Y1*Z1*(XB1*S1+XB2*S2)+S1*(XB3*Y2*Z1+XB4*Y1*Z2)&
                       +XB5*(Y1*Z1*(S3+S1*SI2)+S1*(Z1*Y3+Y1*Z3))

           PZ3 = Z2*(XC1*S1+XC2*S2)+S1*(XC3*Y1*Z2+XC4*Z3)&
                    +XC5*(Z2*(S3+S1*(Y2+SI2))+S1*Z4)

           PY4 = (5.D-1)*(XB1*(Y1*(S3+S1*(Z2+SI2))+S1*Y3)&
                         +XB2*(Y1*(S4+S2*(Z2+SI2))+S2*Y3)&
                         +XB3*(Y2*(S3+S1*(Z2+SI2))+S1*Y4)&
                         +XB4*(Y1*Z1*(S3+S1*SI2)+S1*(Y3*Z1+Y1*Z3))&
                         +XB5*(Y1*(S5+S1*(Z4+SI4))+S1*Y5+2*S3*SYU&
                         +2*S1*Y3*(Z2+SI2)+2*S1*Y1*Z2*SI2))

           PZ4 = (5.D-1)*(XC1*(Z1*(S3+S1*(Y2+SI2))+S1*Z3)&
                         +XC2*(Z1*(S4+S2*(Y2+SI2))+S2*Z3)&
                         +XC3*(Z1*(Y1*(S3+S1*SI2)+S1*Y3)+S1*Y1*Z3)&
                         +XC4*(Z2*(S3+S1*(Y2+SI2))+S1*Z4)&
                         +XC5*(Z1*(S5+S1*(Y4+SI4))+S1*Z5+2*S3*SZU&
                         +2*S1*Y2*(Z3+Z1*SI2)+2*S1*Z3*SI2))          

           TRIU2 = XL*SEU2+YL*SEU3+SEU2*(YXL*Y1+ZXL*Z1)+ZL*(SEU4+SEU2*SXU)&
                  +XR*SGU2+YR*SGU3+SGU2*(YXR*Y1+ZXR*Z1)+ZR*(SGU4+SGU2*SXU)&
                  +(XB1*S1+XB2*S2)*Y1+S1*(XB3*Y2+XB4*Y1*Z1)+XB5*(Y1*(S3+S1*(Z2+SI2))+S1*Y3)&
                  +(XC1*S1+XC2*S2)*Z1+S1*(XC3*Y1*Z1+XC4*Z2)+XC5*(Z1*(S3+S1*(Y2+SI2))+S1*Z3)

           TRIU3 = XL*SEU3+YL*SEU4+SEU3*(YXL*Y1+ZXL*Z1)+ZL*(SEU5+SEU3*SXU)&
                  +XR*SGU3+YR*SGU4+SGU3*(YXR*Y1+ZXR*Z1)+ZR*(SGU5+SGU3*SXU)&
                  +PY1+PZ1
            
           TRIU2Y1 = (XL*SEU2+YL*SEU3)*Y1+SEU2*(YXL*Y2+ZXL*Y1*Z1)&
                     +ZL*(SEU4*Y1+SEU2*SYU)&
                    +(XR*SGU2+YR*SGU3)*Y1+SGU2*(YXR*Y2+ZXR*Y1*Z1)&
                     +ZR*(SGU4*Y1+SGU2*SYU)+PY2+PZ2

           TRIU2Z1 = (XL*SEU2+YL*SEU3)*Z1+SEU2*(YXL*Y1*Z1+ZXL*Z2)&
                     +ZL*(SEU4*Z1+SEU2*SZU)&
                    +(XR*SGU2+YR*SGU3)*Z1+SGU2*(YXR*Y1*Z1+ZXR*Z2)&
                     +ZR*(SGU4*Z1+SGU2*SZU)+PY3+PZ3
              
           TRIEU2 = (5.D-1)*(XL*(SEU4+SEU2*SXU)+YL*(SEU5+SEU3*SXU)&
                            +YXL*(SEU4*Y1+SEU2*SYU)+ZXL*(SEU4*Z1+SEU2*SZU)&
                            +ZL*(SEU6+SEU2*(Y4+Z4+SI4)+2*SEU4*SXU&
                              +2*SEU2*Y2*(Z2+SI2)+2*SEU2*Z2*SI2))&
                   +(5.D-1)*(XR*(SGU4+SGU2*SXU)+YR*(SGU5+SGU3*SXU)&
                            +YXR*(SGU4*Y1+SGU2*SYU)+ZXR*(SGU4*Z1+SGU2*SZU)&
                            +ZR*(SGU6+SGU2*(Y4+Z4+SI4)+2*SGU4*SXU&
                              +2*SGU2*Y2*(Z2+SI2)+2*SGU2*Z2*SI2))&
                   +PY4+PZ4      

!..................................End two slopes for g

!--------------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------

!          Call alternative_A

!--------------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------


           CALL DXE(CK,AXU0,AYU0,AZU0,AE0,AMI,APIX,APIY,APIZ,AEI,A1,B1,C1,D1,E1)
            
! Terms involving \bar(A) needed for the Fluxes

           ATRIU = A1*S1+B1*S2+S1*(C1*Y1+D1*Z1)+E1*(S3+S1*SXU)
           ATRIU2 = A1*S2+B1*S3+S2*(C1*Y1+D1*Z1)+E1*(S4+S2*SXU)
           ATRIUY1 = Y1*(A1*S1+B1*S2)+S1*(C1*Y2+D1*Y1*Z1)+E1*(S3*Y1+S1*SYU)
           ATRIUZ1 = Z1*(A1*S1+B1*S2)+S1*(C1*Y1*Z1+D1*Z2)+E1*(S3*Z1+S1*SZU)
           ATRIEU = (5.D-1)*(A1*(S3+S1*SXU)+B1*(S4+S2*SXU)&
              +C1*(S3*Y1+S1*SYU)+D1*(S3*Z1+S1*SZU)&
              +E1*(S5+S1*(Y4+Z4+SI4)+2*S3*SXU+2*S1*Y2*(Z2+SI2)+2*S1*Z2*SI2))
      
! Prandtl number fix

!           Xc = (5.D-1)*(XL+XR); Yc = (5.D-1)*(YL+YR)
!           YXc = (5.D-1)*(YXL+YXR); YZc = (5.D-1)*(ZXL+ZXR); Zc = (5.D-1)*(ZL+ZR)
           
!           PX1 = Xc*S3+Yc*S4+S3*(YXc*Y1+YZc*Z1)+Zc*(S5+S3*SXU)
!           PX2 = Y1*(Xc*S2+Yc*S3)+S2*(YXc*Y2+YZc*Y1*Z1)+Zc*(S4*Y1+S2*Y3+S2*Y1*(Z2+SI2))
!           PX3 = Z1*(Xc*S2+Yc*S3)+S2*(YXc*Y1*Z1+YZc*Z2)+Zc*(S4*Z1+S2*Z1*(Y2+SI2)+S2*Z3)
!           PX4 = (5.D-1)*(Xc*(S4+S2*SXU)+Yc*(S5+S3*SXU)&
!                +YXc*(S4*Y1+S2*SYU)+YZc*(S4*Z1+S2*SZU)&
!                +Zc*(S6+S2*(Y4+Z4+SI4)+2*S4*SXU+2*S2*Y2*(Z2+SI2)+2*S2*Y2*SI2))
          
!           qs = -TE*(PX4+PY4+PZ4+ATRIEU-AXU0*(PX1+PY1+PZ1+ATRIU2)&
!                   -AYU0*(PX2+PY2+PZ2+ATRIUY1)-AZU0*(PX3+PY3+PZ3+ATRIUZ1))

! Prandtl Number fix - High Mach number
           qs = -TE*(TRIEU2+ATRIEU-AXU0*(TRIU3+ATRIU2)-AYU0*(TRIU2Y1+ATRIUY1)-AZU0*(TRIU2Z1+ATRIUZ1))

           A = ADE*(TAU-SE1)
           B = (5.D-1)*TAU*TAU+SET0

           AFM(i)  = A*S1+SET2*TRIU2+B*ATRIU+FM1+FM2-SETA*FMA
           AFPX(i) = A*S2+SET2*TRIU3+B*ATRIU2+FPX1+FPX2-SETA*FPXA
           AFPY(i) = A*S1*Y1+SET2*TRIU2Y1+B*ATRIUY1+FPY1+FPY2-SETA*FPYA
           AFPZ(i) = A*S1*Z1+SET2*TRIU2Z1+B*ATRIUZ1+FPZ1+FPZ2-SETA*FPZA
           AFE(i)  = (5.D-1)*A*(S3+S1*SXU)+SET2*TRIEU2+B*ATRIEU&
                         +FE1+FE2-SETA*FEA-TAU*(1-1.D0/Pr)*qs
	

         End Do
     Return 
     End Subroutine DXEE

END MODULE gkm_fluxes
