MODULE output

	USE constants

IMPLICIT Double Precision (A-H,O-Z)

CONTAINS

	Subroutine WRITE_FIELD(N_FN,TIME,Cell,gc,CK,GAS_CONSTANT,&
			 IX,&
                          DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,BIX,BIY,BIZ)
!---------------------------------------------------------------------------------------

	Integer, Parameter :: nv=11, n_i=8        !by processor=0 using array size: n_i x IY x IZ (n_i=16 is ok,too)
	Integer :: gc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
                                  XMOM, YMOM, ZMOM, ENER, BAX, BAY, BAZ, BIX, BIY, BIZ
	Character (LEN=30) :: FN

	N_FN = N_FN + 1


	N_FN1 = mod(N_FN,10)
	N_FN2 = mod((N_FN-N_FN1)/10,10)
	write(FN,'(A5,I1,I1,A4)')'field',N_FN2,N_FN1,'.dat'
	Open (Unit=N_unit,File=FN)
	 write(*,*)'Writing data to file: ',FN
!	 Write(N_unit,FMT='(1(A4,X),1(X,A12),3(X,A12),1(X,A12),3(X,A12))') 'i','rho','xMom','yMom',&
!                                        'zMom','e','Bx','By','Bz'
        do i = 1,IX
	   Write(N_unit,FMT='(1(I4,X),8(X,E13.5E3))') i,DENS(i),XMOM(i),YMOM(i),&
					ZMOM(i),ENER(i),BIX(i),BIY(i),BIZ(i)
	end do
	close(N_unit)

	Return
	End Subroutine WRITE_FIELD 
END MODULE output
