#LIBS =						\
	#/Users/Shared/BLAS/blas_LINUX.a		\
	#/Users/Shared/lapack-3.3.1/lapack_OSX.a	\
#####################
#FC = mpif90
#FC = tau_f90.sh # COMPILER FOR TAU INSTRUMENTATION AND PROFILING
FC = gfortran
EXE   = mgkm
#FFLAGS = -O3
FFLAGS = -O3 -Wall -Wtabs -Wno-unused -Wno-unused-dummy-argument -Wno-tabs
#PROFLAG = -pg 	# FLAG FOR GPROF INSTRUMENTATION
VPATH =

.SUFFIXES:
.SUFFIXES: .f90 .o


#SRCMOD =			\
	#mod_constants.f90	\
	#mod_functions.f90	\
	#mod_weno.f90		\
	#mod_boundaries.f90	\
	#mod_energy.f90		\
	#mod_mhd.f90		\
	#mod_initialconditions.f90	\
	#mod_gas-kinetic.f90	\
	#mod_update.f90		\
	#mod_output.f90		\

SRCMOD =			\
	mod_constants.f90	\
	mod_functions.f90	\
	mod_weno.f90		\
	mod_boundaries.f90	\
	mod_mhd.f90		\
	mod_initialconditions.f90	\
	mod_gas-kinetic.f90	\
	mod_update.f90		\
	mod_output.f90		\

SRCMAIN =			\
	main.f90

OBJMAIN = ${SRCMAIN:.f90=.o}

OBJMOD = ${SRCMOD:.f90=.o}

OBJ = $(OBJMOD) $(OBJMAIN)

$(EXE): $(OBJ)
	$(FC) $(LDFLAGS) $(OBJ) $(LIBS) -o $(EXE) $(PROFLAG)
	

%.o  : %.f90 
	$(FC) $(FFLAGS) -c $< $(PROFLAG)

# Define dependencies for modules
$(OBJMAIN) : $(OBJMOD)

clean:
	rm -f *.mod *~ 
	rm -f *.o

veryclean: clean
	rm -f *~ $(EXE)

clear:
	rm -f *~ $(EXE) stderr stdout
