MODULE energy_functions

USE function_module, ONLY : VISCF
USE constants


IMPLICIT Double Precision (A-H,O-Z)
	!Include 'mpif.h'

CONTAINS

Subroutine Initial_Energy(IT,Cell,totalI,gc,GAM,GAS_CONSTANT,CK,TEMP_MEAN,DENS_MEAN,&
	                          VISC,myrank_c,i1,i2,j1,j2,k1,k2,comm3d,temp,DENS,&
	                          XMOM,YMOM,ZMOM,ENER,ek0,dis0,BIX,BIY,BIZ,COND,Bmu_0,B0)
!-------------------------------------------------------------------------------------
	!Implicit Double Precision (A-H,O-Z)
	!Include 'mpif.h'
	Integer :: gc, comm3d
	Double Precision, Dimension(i1-gc:i2+gc,j1-gc:j2+gc,k1-gc:k2+gc) :: DENS,&
	                            XMOM, YMOM, ZMOM, ENER, BIX, BIY, BIZ, temp
	Double Precision :: vect0(9), vect(9)   
	!Double Precision :: VISCF

	Rcell=1.D0/(2*Cell)
	ek=0.0;  eb=0.0
	a=0.0;  enstrophy = 0.0
	ei =0.0; visco = 0.0
	sdis = 0.0; sdis1 = 0.0; sdis2=0.0
      
	Do k=k1,k2
	  Do j=j1,j2
	    Do i=i1,i2
	      Ux = XMOM(i+1,j,k)/DENS(i+1,j,k) - XMOM(i-1,j,k)/DENS(i-1,j,k)
	      Vx = YMOM(i+1,j,k)/DENS(i+1,j,k) - YMOM(i-1,j,k)/DENS(i-1,j,k)
	      Wx = ZMOM(i+1,j,k)/DENS(i+1,j,k) - ZMOM(i-1,j,k)/DENS(i-1,j,k)
             
	      Uy = XMOM(i,j+1,k)/DENS(i,j+1,k) - XMOM(i,j-1,k)/DENS(i,j-1,k)
	      Vy = YMOM(i,j+1,k)/DENS(i,j+1,k) - YMOM(i,j-1,k)/DENS(i,j-1,k)
	      Wy = ZMOM(i,j+1,k)/DENS(i,j+1,k) - ZMOM(i,j-1,k)/DENS(i,j-1,k)
              
	      Uz = XMOM(i,j,k+1)/DENS(i,j,k+1) - XMOM(i,j,k-1)/DENS(i,j,k-1)                            
	      Vz = YMOM(i,j,k+1)/DENS(i,j,k+1) - YMOM(i,j,k-1)/DENS(i,j,k-1)
	      Wz = ZMOM(i,j,k+1)/DENS(i,j,k+1) - ZMOM(i,j,k-1)/DENS(i,j,k-1)

	      S11 = Ux
	      S22 = Vy
	      S33 = Wz
	      S12 = (Uy+Vx)/2
	      S13 = (Uz+Wx)/2
	      S23 = (Vz+Wy)/2


!	      Viscosity

	      visco = visco + VISCF(VISC,TEMP_MEAN,temp(i,j,k))
	      visc1 = VISCF(VISC,TEMP_MEAN,temp(i,j,k)) 

!	      Internal Energy = (CK+3)*DENS(i,j,k)/(4*ELAM), ELAM=1.D0/(2.D0*GAS_CONSTANT*(te0+TEMP_MEAN))

	      eis = (25.D-2)*(CK+3)*DENS(i,j,k)*(2.D0*GAS_CONSTANT*(temp(i,j,k)))
	      ei = ei + eis

!	      Magnetic Field energy = Bmag^2/(2*mu)

	      ebs = (BIX(i,j,k)**2+BIY(i,j,k)**2+BIZ(i,j,k)**2)
	      eb = eb + ebs

!	      Turbulent kinetic energy = 0.5*rho*(u^2+v^2+w^2)
	      eks = (XMOM(i,j,k)*XMOM(i,j,k)+YMOM(i,j,k)*YMOM(i,j,k) &
	            +ZMOM(i,j,k)*ZMOM(i,j,k))/DENS(i,j,k)
	      ek=ek+eks                        ! 2*TKE (with density)
	      a=a+eks/DENS(i,j,k)              ! 2*TKE (no density)

!	      Enstrophy - (fluctuating vorticity)^2 - for solenoidal dissipation
	      enstrophy = enstrophy + ((Wy-Vz)*(Wy-Vz)+(Uz-Wx)*(Uz-Wx)+(Vx-Uy)*(Vx-Uy))

!	      Dissipation = nu*(du_i/dx_j)^2, where nu = visc/rho [visc: dynamic viscosity]
	      sdis = sdis + visc1*(Ux*Ux+Uy*Uy+Uz*Uz + Vx*Vx+Vy*Vy+Vz*Vz + Wx*Wx+Wy*Wy+Wz*Wz)/DENS(i,j,k)
	      !sdis = sdis + visc1*(S11*S11 + S22*S22 + S33*S33+2*(S12*S12 + S13*S13 + S23*S23))/DENS(i,j,k)
	      sdis1 = sdis1 + Ux*Ux+Uy*Uy+Uz*Uz + Vx*Vx+Vy*Vy+Vz*Vz + Wx*Wx+Wy*Wy+Wz*Wz
	      sdis2 = sdis2 + Ux*Ux


	    End Do
	  End Do
	End Do
 
        eb=(5.D-1)*eb*totalI/Bmu_0                    ! BE
	ek=(5.D-1)*ek*totalI                          ! TKE (with density)
	a=a*totalI                                    ! 2*TKE (no density)
	ei = ei*totalI                                ! Internal Energy
	visco = visco*totalI                          ! Viscosity	

	enstrophy= enstrophy*Rcell*Rcell*totalI	      ! Enstrophy (w'w') [w = vorticity]
	dis=Rcell*Rcell*sdis*totalI                   ! Total Dissipation
	!dis=2*Rcell*Rcell*sdis*totalI
	dis1=Rcell*Rcell*sdis1*totalI
	dis2=Rcell*Rcell*sdis2*totalI




	vect(1)=ek; 
	vect(2)=a;  vect(3) = enstrophy 
	vect(4)=eb; vect(5) = ei; vect(6) = visco
	vect(7)=dis; vect(8)=dis1; vect(9)=dis2

	Call MPI_REDUCE(vect,vect0,9,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,ierr)

	ek0=vect0(1)          	    ! TKE (with density)
	a=vect0(2) 	            ! 2*TKE (no density)
	enstrophy = vect0(3)        ! Enstrophy
	eb0 = vect0(4)              ! Magnetic Energy
	ei0 = vect0(5)              ! Internal Energy
	visco0 = vect0(6)           ! Viscosity
	dis0 = vect0(7)             ! Total Dissipation = nu*(du_i/dx_j)^2
	dis4 = vect0(8)
	dis5 = vect0(9)

	If (myrank_c==0) Then	      
	  Write(40,*) 'Mach number=',Dsqrt(a/(GAS_CONSTANT*GAM*TEMP_MEAN))
	  Write(40,*) 'Reynolds number=', 2*ek0/VISC/dsqrt(enstrophy)
	  Write(40,*) 'Reynolds number=', 2*ek0*dsqrt(20./(6*enstrophy))/VISC
	  Write(40,*) 'Magnetic Reynolds number=', 2*ek0*dsqrt(20./(6*enstrophy))*Bmu_0*COND
	  Write(40,*) 'Average rms velocity=',Dsqrt(a/3.D0)
	  Write(40,*) 'Dissipation=', dis0
	
	
	  Write(40,*) TIME,ek0,a,eb0,ei0,enstrophy,dis0,visco0,dis4,dis5
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))/visco0,&
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))*Bmu_0*COND, &
	              !(B0*B0)*COND*dsqrt(20.0/(6.0*enstrophy))
	  !print *,    TIME,ek0,a,eb0,ei0,enstrophy,dis0,visco0,dis4,dis5
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))/visco0,& !4*3.14*ek0/(visco0*dsqrt(a)),&
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))*Bmu_0*COND, &!4*3.14*ek0/(dsqrt(a))*Bmu_0*COND, &
	              !(B0*B0)*COND*dsqrt(20.0/(6.0*enstrophy))
!Ben's Versions	  !Write(40,*) TIME,2*TIME*dis0/a,ek0,eb0,ei0,ek0+eb0+ei0,enstrophy,&    
	  !            dsqrt((20.0/(3.0*visco0*(2.0*visco0*enstrophy))))*ek0, &
	  !            dsqrt((20.0/(3.0*(2.0*enstrophy))))*ek0*COND*Bmu_0, &
	  !            (B0*B0*COND)*(2.0*visco0*enstrophy/ek0)*(Cell*0.061)      
	  !print *,    TIME,2*TIME*dis0/a,ek0,eb0,ei0,ek0+eb0+ei0,enstrophy,&
	  !            dsqrt((20.0/(3.0*visco0*(2.0*visco0*enstrophy))))*ek0, &
	  !            dsqrt((20.0/(3.0*(2.0*enstrophy))))*ek0*COND*Bmu_0, &
	  !            (B0*B0*COND)*(2.0*visco0*enstrophy/ek0)*(Cell*0.061)   
	  !Write(*,*) 'iteration', IT
	  !Write(*,*) 'Turbulent Kinetic Energy= ', ek0
	  !Write(*,*) 'Magnetic Field Energy= ', eb0
	  !Write(*,*) 'Mach number=',Dsqrt(a/(GAS_CONSTANT*GAM*TEMP_MEAN))
	  !Write(*,*) 'Reynolds number=', 2*ek0/VISC/dsqrt(enstrophy)
	  !Write(*,*) 'Reynolds number=' 2*ek0*dsqrt(20./(6*enstrophy))/VISC
	  !Write(*,*) 'Magnetic Reynolds number=', 2*ek0*dsqrt(20./(6*enstrophy))*Bmu_0*COND
	  !Write(*,*) 'Average rms velocity=',Dsqrt(a/3.D0)
	End If

	Return
	End Subroutine Initial_Energy

       
!----------------------------------------------------------------------------
	Subroutine ENERGY(IT,TIME,Cell,totalI,gc,TEMP_MEAN,VISC,myrank_c,i1,i2,&
			  j1,j2,k1,k2,comm3d,ek0,dis0,temp,DENS,XMOM,YMOM,ZMOM,ENER,&
	                  BIX,BIY,BIZ,COND,Bmu_0,GAS_CONSTANT,CK,B0)
!----------------------------------------------------------------------------
	!Implicit Double Precision (A-H,O-Z)
	!Include 'mpif.h'
	Integer :: gc, comm3d
	Double Precision, Dimension(i1-gc:i2+gc,j1-gc:j2+gc,k1-gc:k2+gc) :: DENS,&
	                            XMOM, YMOM, ZMOM, ENER,temp, BIX, BIY, BIZ
	Double Precision :: vect0(9), vect(9)      
	!Double Precision :: VISCF

	Rcell=1.D0/(2*Cell)
	ek=0.0;  eb=0.0
	a=0.0;  enstrophy = 0.0
	ei =0.0; visco = 0.0
	sdis = 0.0; sdis1 = 0.0; sdis2=0.0
      
	Do k=k1,k2
	  Do j=j1,j2
	    Do i=i1,i2
	      Ux = XMOM(i+1,j,k)/DENS(i+1,j,k) - XMOM(i-1,j,k)/DENS(i-1,j,k)

	      Vx = YMOM(i+1,j,k)/DENS(i+1,j,k) - YMOM(i-1,j,k)/DENS(i-1,j,k)
	      Wx = ZMOM(i+1,j,k)/DENS(i+1,j,k) - ZMOM(i-1,j,k)/DENS(i-1,j,k)
             
	      Uy = XMOM(i,j+1,k)/DENS(i,j+1,k) - XMOM(i,j-1,k)/DENS(i,j-1,k)
	      Vy = YMOM(i,j+1,k)/DENS(i,j+1,k) - YMOM(i,j-1,k)/DENS(i,j-1,k)
	      Wy = ZMOM(i,j+1,k)/DENS(i,j+1,k) - ZMOM(i,j-1,k)/DENS(i,j-1,k)
              
	      Uz = XMOM(i,j,k+1)/DENS(i,j,k+1) - XMOM(i,j,k-1)/DENS(i,j,k-1)                            
	      Vz = YMOM(i,j,k+1)/DENS(i,j,k+1) - YMOM(i,j,k-1)/DENS(i,j,k-1)
	      Wz = ZMOM(i,j,k+1)/DENS(i,j,k+1) - ZMOM(i,j,k-1)/DENS(i,j,k-1)

	      S11 = Ux
	      S22 = Vy
	      S33 = Wz
	      S12 = (Uy+Vx)/2
	      S13 = (Uz+Wx)/2
	      S23 = (Vz+Wy)/2




!	      Viscosity

	      visco = visco + VISCF(VISC,TEMP_MEAN,temp(i,j,k))
	      visc1 = VISCF(VISC,TEMP_MEAN,temp(i,j,k))
	  

!	      Internal Energy = (CK+3)*DENS(i,j,k)/(4*ELAM), ELAM=1.D0/(2.D0*GAS_CONSTANT*(te0+TEMP_MEAN))

	      eis = (25.D-2)*(CK+3)*DENS(i,j,k)*(2.D0*GAS_CONSTANT*(temp(i,j,k)))
	      ei = ei + eis

!	      Magnetic Field energy = Bmag^2/(2*mu)

	      ebs = (BIX(i,j,k)**2+BIY(i,j,k)**2+BIZ(i,j,k)**2)
	      eb = eb + ebs

!	      Turbulent kinetic energy = 0.5*rho*(u^2+v^2+w^2)
	      eks = (XMOM(i,j,k)*XMOM(i,j,k)+YMOM(i,j,k)*YMOM(i,j,k) &
	            +ZMOM(i,j,k)*ZMOM(i,j,k))/DENS(i,j,k)
	      ek=ek+eks                        ! 2*TKE (with density)
	      a=a+eks/DENS(i,j,k)              ! 2*TKE (no density)

!	      Enstrophy - (fluctuating vorticity)^2 - for solenoidal dissipation
	      enstrophy = enstrophy + ((Wy-Vz)*(Wy-Vz)+(Uz-Wx)*(Uz-Wx)+(Vx-Uy)*(Vx-Uy))

!	      Dissipation = nu*(du_i/dx_j)^2, where nu = visc/rho [visc: dynamic viscosity]
	      sdis = sdis + visc1*(Ux*Ux+Uy*Uy+Uz*Uz + Vx*Vx+Vy*Vy+Vz*Vz + Wx*Wx+Wy*Wy+Wz*Wz)/DENS(i,j,k)
	      !sdis = sdis + visc1*(S11*S11 + S22*S22 + S33*S33+2*(S12*S12 + S13*S13 + S23*S23))/DENS(i,j,k)
	      sdis1 = sdis1 + Ux*Ux+Uy*Uy+Uz*Uz + Vx*Vx+Vy*Vy+Vz*Vz + Wx*Wx+Wy*Wy+Wz*Wz
	      sdis2 = sdis2 + Ux*Ux


	    End Do
	  End Do
	End Do
 
        eb=(5.D-1)*eb*totalI/Bmu_0                    ! BE
	ek=(5.D-1)*ek*totalI                          ! TKE (with density)
	a=a*totalI                                    ! 2*TKE (no density)
	ei = ei*totalI                                ! Internal Energy
	visco = visco*totalI                          ! Viscosity	

	enstrophy= enstrophy*Rcell*Rcell*totalI	      ! Enstrophy (w'w') [w = vorticity]
	dis=Rcell*Rcell*sdis*totalI                   ! Total Dissipation
	!dis=2*Rcell*Rcell*sdis*totalI
	dis1=Rcell*Rcell*sdis1*totalI
	dis2=Rcell*Rcell*sdis2*totalI


	vect(1)=ek; 
	vect(2)=a;  vect(3) = enstrophy 
	vect(4)=eb; vect(5) = ei; vect(6) = visco
	vect(7)=dis; vect(8)=dis1; vect(9)=dis2

	Call MPI_REDUCE(vect,vect0,9,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,ierr)

	ek0=vect0(1)          	    ! TKE (with density)
	a=vect0(2) 	            ! 2*TKE (no density)
	enstrophy = vect0(3)        ! Enstrophy
	eb0 = vect0(4)              ! Magnetic Energy

	ei0 = vect0(5)              ! Internal Energy
	visco0 = vect0(6)           ! Viscosity
	dis0 = vect0(7)             ! Total Dissipation = nu*(du_i/dx_j)^2
	dis4 = vect0(8)
	dis5 = vect0(9)

	If (myrank_c==0.and.MOD(IT,10000)==0) Then	
	
	  Write(40,*) TIME,ek0,a,eb0,ei0,enstrophy,dis0,visco0,dis4,dis5
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))/visco0,&
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))*Bmu_0*COND,(B0*B0)*COND*2.0*3.14/dsqrt(a)
	  print *,    TIME,ek0,a,eb0,ei0,enstrophy,dis0,visco0,dis4,dis5
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))/visco0,&
	              !2.0*ek0*dsqrt(20.0/(6.0*enstrophy))*Bmu_0*COND,(B0*B0)*COND*2.0*3.14/dsqrt(a)
!Ben's Versions	  !Write(40,*) TIME,2*TIME*dis0/a,ek0,eb0,ei0,ek0+eb0+ei0,enstrophy,&    
	  !            dsqrt((20.0/(3.0*visco0*(2.0*visco0*enstrophy))))*ek0, &
	  !            dsqrt((20.0/(3.0*(2.0*enstrophy))))*ek0*COND*Bmu_0, &
	  !            (B0*B0*COND)*(2.0*visco0*enstrophy/ek0)*(Cell*0.061)      
	  !print *,    TIME,2*TIME*dis0/a,ek0,eb0,ei0,ek0+eb0+ei0,enstrophy,&
	  !            dsqrt((20.0/(3.0*visco0*(2.0*visco0*enstrophy))))*ek0, &
	  !            dsqrt((20.0/(3.0*(2.0*enstrophy))))*ek0*COND*Bmu_0, &
	  !            (B0*B0*COND)*(2.0*visco0*enstrophy/ek0)*(Cell*0.061)   

	  !Write(*,*) 'iteration', IT
	  !Write(*,*) 'iteration', IT
	  !Write(*,*) 'Turbulent Kinetic Energy= ', ek0
	  !Write(*,*) 'Magnetic Field Energy= ', eb0
	  !Write(*,*) 'Mach number=',Dsqrt(a/(GAS_CONSTANT*GAM*TEMP_MEAN))
	  !Write(*,*) 'Reynolds number=', 2*ek0/VISC/dsqrt(enstrophy)
	  !Write(*,*) 'Reynolds number=' 2*ek0*dsqrt(20./(6*enstrophy))/VISC
	  !Write(*,*) 'Magnetic Reynolds number=', 2*ek0*dsqrt(20./(6*enstrophy))*Bmu_0*COND
	  !Write(*,*) 'Average rms velocity=',Dsqrt(a/3.D0)
	End If
	Return
	End Subroutine ENERGY 

END MODULE energy_functions
