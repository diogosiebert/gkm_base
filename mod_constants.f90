! This file contains some general constants & parameters
! Any module which requires them should USE this module
MODULE constants
	implicit NONE

!#######   PHYSICAL CONSTANTS   ##########!
	! All units are SI
	double precision, parameter :: PI=3.141592653589739D0
	double precision, parameter :: kappa = 1.3806503D-23 ! Boltzmann's constant
	double precision, parameter :: Avag = 6.0221415D23   ! Avogadro's Number
	double precision, parameter :: Q_elec = 1.60217646D-19 ! Unit charge (electron)
	double precision, parameter :: Bmu_0 = 4.0D0*PI*1.D-7 ! Magnetic permeability
	double precision, parameter :: R_GAS = 8.3144621 ! Universal Gas Constant
	!double precision, parameter :: c_ratio = 0.18D0 ! for divB control, see Dedner et. al (Hyper. Div Cleaning)

!####### COMPUTATIONAL CONSTANTS #########!
	integer, parameter :: gc = 3 	! no. of ghost cells
	!double precision, parameter :: Hall_iter = 100 	! no. of Hall sub-cycles
END MODULE constants
