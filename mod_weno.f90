MODULE WENO

IMPLICIT Double Precision (A-H,O-Z)

CONTAINS

     Subroutine weno_x(gc,IX,Q,QL,QR)
!-------------------------------------------------------------------------------
      Integer, Parameter :: ig=2     !(weno r=3)
      Integer :: gc
      Double Precision, Dimension(0:ig) :: d,Q1,Q2,beta1,beta2,alpha1,alpha2,omega1,omega2
      Double Precision, Dimension(1-gc:IX+gc) :: Q,QL,QR

	epsilon = 1.D-12					! To avoid division by zero
	a = 1.0D0/6.0D0
	b = 13.D0/12.D0
	c = 1.0D0/4.0D0

!	Weno coefficients
	d(0) = 0.1D0
	d(1) = 0.6D0
	d(2) = 0.3D0
      
          Do i=0,IX
	  Q1(0) = a*( 2.D0*Q(i-2)-7.D0*Q(i-1)+11.D0*Q(i))
	  Q1(1) = a*(-1.D0*Q(i-1)+5.D0*Q(i)  + 2.D0*Q(i+1))
	  Q1(2) = a*( 2.D0*Q(i)  +5.D0*Q(i+1)- 1.D0*Q(i+2))

	  Q2(0) = a*( 2.D0*Q(i+3)-7.D0*Q(i+2)+11.D0*Q(i+1))
	  Q2(1) = a*(-1.D0*Q(i+2)+5.D0*Q(i+1)+ 2.D0*Q(i))
	  Q2(2) = a*( 2.D0*Q(i+1)+5.D0*Q(i)  - 1.D0*Q(i-1))

	  beta1(0) = b*(Q(i-2)-2.D0*Q(i-1)+Q(i))**2   + c*(Q(i-2)-4.D0*Q(i-1)+3.D0*Q(i))**2
	  beta1(1) = b*(Q(i-1)-2.D0*Q(i)  +Q(i+1))**2 + c*(-Q(i-1)+Q(i+1))**2
	  beta1(2) = b*(Q(i)  -2.D0*Q(i+1)+Q(i+2))**2 + c*(3.D0*Q(i)-4.D0*Q(i+1)+Q(i+2))**2

	  beta2(0) = b*(Q(i+3)-2.D0*Q(i+2)+Q(i+1))**2 + c*(Q(i+3)-4.D0*Q(i+2)+3.D0*Q(i+1))**2
	  beta2(1) = b*(Q(i+2)-2.D0*Q(i+1)+Q(i))**2   + c*(-Q(i+2)+Q(i))**2
	  beta2(2) = b*(Q(i+1)-2.D0*Q(i)  +Q(i-1))**2 + c*(3.D0*Q(i+1)-4.D0*Q(i)+Q(i-1))**2
	  alphaL=0.D0; alphaR=0.D0

            Do jj=0,ig
              alpha1(jj) = d(jj)/(beta1(jj)+epsilon)**2
              alpha2(jj) = d(jj)/(beta2(jj)+epsilon)**2

              alphaL = alphaL + alpha1(jj)
              alphaR = alphaR + alpha2(jj)
            End Do
             
            Do jj=0,ig
              omega1(jj) = alpha1(jj)/alphaL
              omega2(jj) = alpha2(jj)/alphaR
            End Do

            sL=0.D0;  sR=0.D0
            Do jj=0,ig
              sL = sL + omega1(jj)*Q1(jj)
              sR = sR + omega2(jj)*Q2(jj)
           End Do

           QL(i+1)=sL
           QR(i+1)=sR
          End Do
        !End Do
      !End Do
  
      Return
      End Subroutine weno_x

!###############################################################
!######### SYMMETRIC WENO FOR MHD INTERPOLATING  ###############
!###############################################################
!-------------------------------------------------------------------------------
     Subroutine wenosym_x(gc,IX,Q,QL,QR)
!-------------------------------------------------------------------------------
      Integer, Parameter :: ig=3     !(weno r=3)
      Integer :: gc
      Double Precision, Dimension(0:ig) :: d,Q1,Q2,beta1,beta2,alpha1,alpha2,omega1,omega2
      Double Precision, Dimension(1-gc:IX+gc) :: Q,QL,QR

	epsilon = 1.D-12					! To avoid division by zero
	a = 1.0D0/6.0D0
	b = 13.D0/12.D0
	c = 1.0D0/4.0D0

!	Weno coefficients
	! Order-Optimized
	d(0) = 0.05D0
	d(1) = 0.45D0
	d(2) = 0.45D0
	d(3) = 0.05D0
	!! Bandwidth-Optimized
	!d(0) = 0.094647545896D0
	!d(1) = 0.428074212384D0
	!d(2) = 0.408289331408D0
	!d(3) = 0.068988910311D0
      
          Do i=0,IX

	  Q1(0) = a*( 2.D0*Q(i-2)-7.D0*Q(i-1)+11.D0*Q(i))
	  Q1(1) = a*(-1.D0*Q(i-1)+5.D0*Q(i)  + 2.D0*Q(i+1))
	  Q1(2) = a*( 2.D0*Q(i)  +5.D0*Q(i+1)- 1.D0*Q(i+2))
	  Q1(3) = a*( 11.D0*Q(i+1)  -7.D0*Q(i+2)+ 2.D0*Q(i+3))

	  Q2(0) = a*( 2.D0*Q(i+3)-7.D0*Q(i+2)+11.D0*Q(i+1))
	  Q2(1) = a*(-1.D0*Q(i+2)+5.D0*Q(i+1)+ 2.D0*Q(i))
	  Q2(2) = a*( 2.D0*Q(i+1)+5.D0*Q(i)  - 1.D0*Q(i-1))
	  Q2(3) = a*( 11.D0*Q(i)-7.D0*Q(i-1)  + 2.D0*Q(i-2))

	  beta1(0) = b*(Q(i-2)-2.D0*Q(i-1)+Q(i))**2   &
		   + c*(Q(i-2)-4.D0*Q(i-1)+3.D0*Q(i))**2
	  beta1(1) = b*(Q(i-1)-2.D0*Q(i)  +Q(i+1))**2 &
		   + c*(-Q(i-1)+Q(i+1))**2
	  beta1(2) = b*(Q(i)  -2.D0*Q(i+1)+Q(i+2))**2 &
		   + c*(3.D0*Q(i)-4.D0*Q(i+1)+Q(i+2))**2
	  beta1(3) = b*(Q(i+1)  -2.D0*Q(i+2)+Q(i+3))**2 &
		   + c*(5.D0*Q(i+1)-8.D0*Q(i+2)+3.D0*Q(i+3))**2
	  beta1(3) = DMAX1(beta1(0),beta1(1),beta1(2),beta1(3))

	  beta2(0) = b*(Q(i+3)-2.D0*Q(i+2)+Q(i+1))**2   &
		   + c*(Q(i+3)-4.D0*Q(i+2)+3.D0*Q(i+1))**2
	  beta2(1) = b*(Q(i+2)-2.D0*Q(i+1)  +Q(i))**2 &
		   + c*(-Q(i+2)+Q(i))**2
	  beta2(2) = b*(Q(i+1)  -2.D0*Q(i)+Q(i-1))**2 &
		   + c*(3.D0*Q(i+1)-4.D0*Q(i)+Q(i-1))**2
	  beta2(3) = b*(Q(i)  -2.D0*Q(i-1)+Q(i-2))**2 &
		   + c*(5.D0*Q(i)-8.D0*Q(i-1)+3.D0*Q(i-2))**2
	  beta2(3) = DMAX1(beta2(0),beta2(1),beta2(2),beta2(3))

	  alphaL=0.D0
	  alphaR=0.D0
            Do jj=0,ig
              alpha1(jj) = d(jj)/(beta1(jj)+epsilon)**2
              alphaL = alphaL + alpha1(jj)
              alpha2(jj) = d(jj)/(beta2(jj)+epsilon)**2
              alphaR = alphaR + alpha2(jj)
            End Do
            Do jj=0,ig
              omega1(jj) = alpha1(jj)/alphaL
              omega2(jj) = alpha2(jj)/alphaR
            End Do
            sL=0.D0
            sR=0.D0
            Do jj=0,ig
              sL = sL + omega1(jj)*Q1(jj)
              sR = sR + omega2(jj)*Q2(jj)
           End Do

           QL(i+1)=sL
           QR(i+1)=sR
          End Do
  
      Return
      End Subroutine wenosym_x

END MODULE WENO
