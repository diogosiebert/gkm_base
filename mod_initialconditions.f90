MODULE initial_conditions

USE constants

IMPLICIT Double Precision (A-H,O-Z)

CONTAINS

Subroutine INPUT(gc,GAM,CK,GAS_CONSTANT,TEMP_MEAN,DENS_MEAN,&
	                 IX,&
	                 DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,BIX,BIY,BIZ,deltaP,&
	                 VISC,Cell,U_in,COND,B0,Ha,Rmag,U_char,U_wall,Bmu_0)
!--------------------------------------------------------------------------------
	Integer, parameter :: nv=11
	Integer :: gc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
	                            XMOM, YMOM, ZMOM, ENER, BAX, BAY, BAZ, BIX, BIY, BIZ
	Double Precision, Dimension(1-gc:IX+gc) :: BA_X,BA_Y,BA_Z

	RHOL = 1.D0
	RHOR = 1.25D-1

	!BXL = 0.D0
	!BYL = 0.D0
	!BXR = 0.D0
	!BYR = 0.D0
	BXL = 0.75D0*dsqrt(Bmu_0)
	BXR = BXL
	BYL = 1.D0*dsqrt(Bmu_0)
	BYR = -1.D0*dsqrt(Bmu_0)

	BZL = 0.D0
	BZR = 0.D0

	! BrioWu
	!PL = 1.D0
	!PR = 0.1D0
	TL = 1.579D1
	TR = 1.2633D1

	!! Srini
	!TL = 7.8956D0
	!TR = 6.3165D0

	!! Shuml
	!TL = 3.4833D1
	!TR = 2.78669D1

	Do i=1-gc,IX+gc
	            BAX(i) = 0.D0
	            BAY(i) = 0.D0
	            BAZ(i) = 0.D0
	end do
	Do i=1,IX
		If(i<(IX/2+1)) then
                    DENS(i) = RHOL
	            XMOM(i) = 0.D0
                    YMOM(i) = 0.D0
                    ZMOM(i) = 0.D0
	               !ELAM = RHOL/(2.D0*PL) !Enery = KE + IE
	               ELAM = 1.D0/(2.D0*GAS_CONSTANT*TL) !Enery = KE + IE
	            ENER(i) = (5.D-1)*(XMOM(i)**2+YMOM(i)**2&
	                        +ZMOM(i)**2)/DENS(i)&
	                        +(CK+3)*DENS(i)/(4*ELAM)
	            BIX(i) = BXL
	            BIY(i) = BYL
	            BIZ(i) = BZL
		else
                    DENS(i) = RHOR
	            XMOM(i) = 0.D0
                    YMOM(i) = 0.D0
                    ZMOM(i) = 0.D0
	               !ELAM = RHOR/(2.D0*PR) !Enery = KE + IE
	               ELAM = 1.D0/(2.D0*GAS_CONSTANT*TR) !Enery = KE + IE
	            ENER(i) = (5.D-1)*(XMOM(i)**2+YMOM(i)**2&
	                        +ZMOM(i)**2)/DENS(i)&
	                        +(CK+3)*DENS(i)/(4*ELAM)
	            BIX(i) = BXR
	            BIY(i) = BYR
	            BIZ(i) = BZR
		end if
	             If (ENER(i)<=0.0) Then
	               Write(*,*)  'Negative energy'
			Stop
	             End If
	end do

	Return
	End Subroutine INPUT

END MODULE initial_conditions
