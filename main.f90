! Main MGKM program file
! most recent edit: 2014/05/30 - Steven Anderson
! 
Program mgkm

	USE function_module	! functions (erf/erfc/vsicf...)
	USE boundary_cond	! sets the boundary conditions
	USE update_sub		! does the update of the density/momentum/etc.
	USE output		! Writes output to file
	USE initial_conditions	! Sets initial conditions over the domain
	USE constants		! Some constants (Pi, kappa, etc.)

	Implicit Double Precision (A-H,O-Z)
	Double Precision, Allocatable, Dimension(:) :: DENS, XMOM, YMOM, ZMOM, ENER, temp,&
	                                                   BAX, BAY, BAZ, BIX, BIY, BIZ,PSI,PSI2,&
							   XMOM2,YMOM2,ZMOM2,ENER2,BIX2,BIY2,BIZ2
	Integer :: SSIT
	Logical :: periods(0:2)
	Logical :: HALLMHD,initial_out,fwrite

	! variables for initializing input file
	character(len=100) :: fname_in, bname_in, Bfname_in
	integer ::  handle = 00,myhandle ! Handle counter for opening files
	integer :: ios = 00
	character(len=100) :: buffer, label

	! variables from control file
	integer :: IX,IY,IZ	!Number of cells in each direction
	integer :: iprocs,kprocs,jprocs,nprocs ! num of procs assigned to each dir. 
	integer :: IMAX !Max # timesteps
	integer :: EXTENSION, fnum_out, write_multi
		!Ideally, should be based on CFL condition, w/ dynamic timestep
	Double Precision :: MOL_MASS !constants
	Double Precision :: MAX_T,Lchar

!--------------------------------------------------------------------------------------
!           setting flow properties
!--------------------------------------------------------------------------------------

		! Open the config file, and pass to function for reading
	Call config(IX,Cell,TAU,&
		IMAX,GAM,MOL_MASS,TEMPchar,DENSchar,Lchar,Pr,VISC,COND,B0,&
		HALLMHD,MAX_T,fnum_out,initial_out,&
		CFL,CFL_H,Uchar,gradP,c_ratio,ch_mult)

 	CK = (5-3*GAM)/(GAM-1)
	GAS_CONSTANT = R_GAS/MOL_MASS    !Gas Constant

	R3 = 1.D0/3.D0

      
!--------------------------------------------------------------------------------------
	Allocate(DENS(1-gc:IX+gc),XMOM(1-gc:IX+gc),&
                YMOM(1-gc:IX+gc),ZMOM(1-gc:IX+gc),&
                ENER(1-gc:IX+gc),temp(1-gc:IX+gc),&
		BAX(1-gc:IX+gc),BAY(1-gc:IX+gc),BAZ(1-gc:IX+gc),&
		BIX(1-gc:IX+gc),BIY(1-gc:IX+gc),&
		BIZ(1-gc:IX+gc),PSI(1-gc:IX+gc))
	Allocate(XMOM2(1-gc:IX+gc),YMOM2(1-gc:IX+gc),&
                ZMOM2(1-gc:IX+gc),ENER2(1-gc:IX+gc),&
		BIX2(1-gc:IX+gc),BIY2(1-gc:IX+gc),&
		BIZ2(1-gc:IX+gc),PSI2(1-gc:IX+gc))

!--------------------------------------------------------------------------------------
	TIME = 0.D0
	IT   = 0
	eta  = 1.0   ! Used to Scale velocity field
	etab = 0.0   ! Used to scale magnetic field
	N_FN = 0
	    if (HALLMHD) then
		Write(*,*) 'HALLMHD set to "ON"'
	    else
	    	write(*,*) 'HALLMHD set to "OFF"'
	    end if
	    
!--------------------------------------------------------------------------------------

!-------Input Subroutine
	Call INPUT(gc,GAM,CK,GAS_CONSTANT,TEMPchar,DENSchar,&   
	           IX,&
	           DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,BIX,BIY,BIZ,deltaP,VISC,&
	           Cell,U_in,COND,B0,Ha,Rmag,U_char,U_wall,Bmu_0)
			
	! init cond on PSI set to zero everywhere

	PSI = 0.D0

!-------Boundary Condition Subroutine    
	Call BOUNDARY(CK,GAS_CONSTANT,DENSchar,TEMPchar,Lchar,gc,&
	              IX,DENS,XMOM,&
	              YMOM,ZMOM,ENER,temp,gradP,Cell,GAM,VISC,Uchar,&
	              BIX,BIY,BIZ,PSI,Bmu_0,COND,B0,&
	              periods)
!--------------------------------------------------------------------------------------

	totalI=1.D0/(IX)

        !Call Initial_Energy(IT,Cell,totalI,gc,GAM,GAS_CONSTANT,CK,TEMPchar,DENSchar,&
        !                   VISC,myrank_c,ista,iend,jsta,jend,ksta,kend,comm3d,&
        !                   temp,DENS,XMOM,YMOM,ZMOM,ENER,ek0,dis0,BIX,BIY,BIZ,COND,Bmu_0,B0)

	if(initial_out) then
	    N_FN = -1 !WRITE_FIELD increments N_FN by +1, initial out should have N_FN=0
	    Call WRITE_FIELD(N_FN,TIME,Cell,gc,CK,GAS_CONSTANT,&
			 IX,&
                         DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,BIX,BIY,BIZ)
	end if


!------------------------------------------------------------------------------
	TAU_eps = 1.D-10
	fout_delay = MAX_T/fnum_out
	Do           
	  !If (IT==IMAX) Exit
	  If (TIME.ge.MAX_T) Exit
!------------------------------------------------------------------------------
	  Call COURANT(Cell,GAM,CK,TEMPchar,VISC,gc,IX,&
	               temp,DENS,XMOM,YMOM,ZMOM,ENER,& 
	               BAX,BAY,BAZ,BIX,BIY,BIZ,Bmu_0,VMAX)
	  VCFL = VMAX
	  TAU_OLD = TAU
	  TAU = CFL*CELL*R3/VCFL ! 3*vel
	   if(DMOD(TIME+TAU,fout_delay).lt.DMOD(TIME,fout_delay)) then
	      TAU = fout_delay - DMOD(TIME,fout_delay) + TAU_eps*MAX_T
	      fwrite = .true.
	   else if((TIME + TAU).gt.(MAX_T)) then
	      TAU = (1.D0+TAU_eps)*MAX_T-TIME
	   end if
	  TIME = TIME+TAU
	  IT   = IT+1
	  alpha = TAU/TAU_OLD

	  Call COURANT_HALL(Cell,GAM,CK,TEMPchar,VISC,MOL_MASS,gc,IX,&
	               temp,DENS,XMOM,YMOM,ZMOM,ENER,& 
	               BAX,BAY,BAZ,BIX,BIY,BIZ,Bmu_0,VMAXH)
	  VCFLH = VMAXH
	  TAU_H = CFL_H*CELL*R3/VCFLH

!---------Update subrouting in which cell center values are updated
	  Call UPDATE(TAU,TAU_H,alpha,Cell,gc,CK,GAS_CONSTANT,Pr,TEMPchar,&
		DENSchar,gradP,VISC,IX,&
	         temp,DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,&
		 XMOM2,YMOM2,ZMOM2,ENER2,BIX2,BIY2,BIZ2,PSI2,&
	         Bmu_0,COND,B0,Uchar,IT,Lchar,MOL_MASS,Avag,Q_elec,kappa,&
	         HALLMHD,CFL,SSIT,vcfl,vcflh,&
		 c_ratio,ch_mult,periods)

!---------Boundary subroutine in which Boundary conditions for each cell are set for next iteration
	Call BOUNDARY(CK,GAS_CONSTANT,DENSchar,TEMPchar,Lchar,gc,&
	              IX,DENS,XMOM,&
	              YMOM,ZMOM,ENER,temp,gradP,Cell,GAM,VISC,Uchar,&
	              BIX,BIY,BIZ,PSI,Bmu_0,COND,B0,&
	              periods)

	  !Call ENERGY(IT,TIME,Cell,totalI,gc,TEMPchar,VISC,myrank_c,ista,iend,jsta,&
	  !            jend,ksta,kend,comm3d,ek0,dis0,temp,DENS,XMOM,YMOM,ZMOM,ENER, &
	  !            BIX,BIY,BIZ,COND,Bmu_0,GAS_CONSTANT,CK,B0)

	  If (DMOD(TIME+TAU,1.D-2*MAX_T).lt.DMOD(TIME,1.D-2*MAX_T)) then
		Write(*,'(2I10,2E12.3)') IT,SSIT,TAU,TIME
	  end if

	  If (fwrite) then
	    Call WRITE_FIELD(N_FN,TIME,Cell,gc,CK,GAS_CONSTANT,&
			 IX,&
                         DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,BIX,BIY,BIZ)
	    fwrite = .false.
	  End If

!------------------------------------------------------------------------------   
	End Do
!------------------------------------------------------------------------------

	Deallocate(DENS,XMOM,YMOM,ZMOM,ENER,temp,BAX,BAY,BAZ,BIX,BIY,BIZ,PSI)
	Deallocate(XMOM2,YMOM2,ZMOM2,ENER2,BIX2,BIY2,BIZ2)

	End Program mgkm
