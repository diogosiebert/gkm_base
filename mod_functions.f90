! Module which holds functions used by other portions of the program
! Function List:
!	i)   ERF(X): error function
!	ii)  ERFC(X): complementary error function
!	iii) VISCF(T): function to vary viscosity with temperature
!	iv)  S_EQI(s1,s2): function which compares strings for case-insensitive equality
!	v)   CH_CAP(c): subroutine which capitalizes a single character
!	vi)  config(args..): subroutine which opens and reads the mgkm.conf file
!	vii)  para_range: subroutine which returns the start/end points of the
!			 block of cells which each process controls
!	viii)   para_type_block3: does some MPI stuff with parameter types(?)
!	ix)  COURANT: subroutine for enforcing the CFL constraint 
!	x)  COURANT_HALL: subroutine for enforcing the CFL constraint for HALL sub-cycling
!
! Last edit: 2014/05/31 - Steven Anderson

MODULE function_module

USE constants	!Constants, (Pi, etc)

IMPLICIT Double Precision (A-H,O-Z)
        !INCLUDE 'mpif.h'

CONTAINS

			

        Double Precision Function DERFC(X)
        Double Precision :: X, ERF
        DERFC=1.D0-ERF(X)
        Return
        End Function DERFC
 

        Double Precision Function ERF(X)
        Implicit Double Precision (A-H,O-Z)
        EPS=1.0D-9
        X2=X*X
        If (DABS(X)<3.5) Then
          ER=1.D0
          R=1.D0
          Do K=1,50
            R=R*X2/(K+5.D-1)
            ER=ER+R
            If (DABS(R)<=DABS(ER)*EPS) Exit
          End Do
          C0=2.D0/DSQRT(PI)*X*DEXP(-X2)
          ERF=C0*ER
        Else
           ER=1.D0
           R=1.D0
           Do K=1,12
             R=-R*(K-5.D-1)/X2
             ER=ER+R
           End Do
           C0=DEXP(-X2)/(DABS(X)*DSQRT(PI))

           ERF=1.D0-C0*ER
           If (X<0.0) ERF=-ERF
        End If
!       DERFC=1.D0-ERF
        Return
        End Function ERF


        Double Precision Function VISCF(visc0,temp0,temperature)
        Implicit Double Precision (A-H,O-Z)
       !T=1.D0/(2*287*ELAM)
       !VISCF=((8.3D-5)*(291.15D0 + 120.D0)/(T+120.D0))*(T/291.15D0)**(1.5)
        !VISCF=visc0*(temperature/temp0)**0.76
        VISCF=visc0
        Return
        End Function VISCF

	Function S_EQI ( s1, s2 )

!*****************************************************************************80
!! S_EQI is a case insensitive comparison of two strings for equality.
! obtained from: http://people.sc.fsu.edu/~jburkardt/f_src
!
!  Example:
!    S_EQI ( 'Anjana', 'ANJANA' ) is .TRUE.
!
!  Licensing:
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!    14 April 1999
!  Author:
!    John Burkardt
!  Parameters:
!    Input, character ( len = * ) S1, S2, the strings to compare.
!    Output, logical S_EQI, the result of the comparison.
!
  	  implicit none

	  character c1
  	  character c2
	  integer ( kind = 4 ) i
	  integer ( kind = 4 ) len1
	  integer ( kind = 4 ) len2
	  integer ( kind = 4 ) lenc
	  logical s_eqi
	  character ( len = * ) s1
	  character ( len = * ) s2

	  len1 = len ( s1 )
	  len2 = len ( s2 )
	  lenc = min ( len1, len2 )

	  s_eqi = .false.

	  do i = 1, lenc

	    c1 = s1(i:i)
	    c2 = s2(i:i)
	    call ch_cap ( c1 )
	    call ch_cap ( c2 )

	    if ( c1 /= c2 ) then
	      return
	    end if

	  end do

	  do i = lenc + 1, len1
	    if ( s1(i:i) /= ' ' ) then
	      return
	    end if
	  end do

	  do i = lenc + 1, len2
	    if ( s2(i:i) /= ' ' ) then
	      return
	    end if
	  end do

	  s_eqi = .true.

	  return
	END FUNCTION S_EQI

	subroutine CH_CAP ( c )

	!*****************************************************************************80
	!
	!! CH_CAP capitalizes a single character.
	! obtained from: http://people.sc.fsu.edu/~jburkardt/f_src
	!
	!  Licensing:
	!
	!    This code is distributed under the GNU LGPL license.
	!
	!  Modified:
	!
	!    19 July 1998
	!
	!  Author:
	!
	!    John Burkardt
	!
	!  Parameters:
	!
	!    Input/output, character C, the character to capitalize.
	!
	  implicit none

	  character c
	  integer ( kind = 4 ) itemp

	  itemp = ichar ( c )

	  if ( 97 <= itemp .and. itemp <= 122 ) then
	    c = char ( itemp - 32 )
	  end if

	  return
	END SUBROUTINE CH_CAP

	Subroutine config(IX,Cell,TAU,&
			IMAX,GAM,MOL_MASS,TEMPchar,DENSchar,Lchar,Pr,VISC,COND,B0,&
			HALLMHD,MAX_T,fnum_out,initial_out,&
			CFL,CFL_H,Uchar,gradP,c_ratio,ch_mult)
		!Input vars
		double precision :: MOL_MASS,MAX_T,CFL,Lchar
		character(len=100) :: fname_in,bname_in,Bfname_in
		integer :: iprocs, jprocs, kprocs
		integer :: IX, IY, IZ, IMAX
		integer :: EXTENSION, fnum_out, write_multi
		character(len=5) :: hall_string
		character(len=10) :: write_string
		logical :: HALLMHD,initial_out

		!File read variables
		integer :: handle,myhandle,pos
		character(len=100) :: label, buffer
		character :: c
		integer :: ios = 0
		integer :: line = 0
		integer :: length, i
		
		! DEFAULT VALUES 
		CFL = 0.5D0
		CFL_H = 0.5D0
		! ################
		myhandle = handle
		handle = handle + 10
		Open (Unit=myhandle,File='mgkm.conf',status='OLD',action='READ',iostat=ios)

			if(ios>0) then
				print *, "ERROR OPENING CONFIG FILE"
	  			!Call MPI_ABORT(MPI_COMM_WORLD,ierr)
				Stop
			end if
		initial_out = .FALSE.
		do while (ios == 0)
			read(myhandle,'(A)',iostat=ios) buffer
			if (ios==0) then
				line = line + 1
				if ((buffer(1:1)=='#').or.(buffer=='')) cycle

				! Find whitespace, split the label/data
				pos = scan(buffer,' ')
				label = buffer(1:pos)
				buffer = buffer(pos+1:)

	  			length = LEN(label)
				do i = 1,length
	    			    c = label(i:i)
				    CALL ch_cap(c)
				    label(i:i) = c
				end do

				select case(label)
				case('IX')
					read(buffer, *, iostat=ios) IX
				case('IY')
					read(buffer, *, iostat=ios) IY
				case('IZ')
					read(buffer, *, iostat=ios) IZ
				case('IPROCS')
					read(buffer, *, iostat=ios) iprocs
				case('JPROCS')
					read(buffer, *, iostat=ios) jprocs
				case('KPROCS')
					read(buffer, *, iostat=ios) kprocs
				case('CELL')
					read(buffer, *, iostat=ios) cell
				case('TAU')
					read(buffer, *, iostat=ios) TAU
				case('IMAX')
					read(buffer, *, iostat=ios) IMAX
				case('GAM')
					read(buffer, *, iostat=ios) GAM
				case('MOL_MASS')
					read(buffer, *, iostat=ios) MOL_MASS
				case('TEMPCHAR')
					read(buffer, *, iostat=ios) TEMPchar
				case('DENSCHAR')
					read(buffer, *, iostat=ios) DENSchar
				case('GRADP')
					read(buffer, *, iostat=ios) gradP
				case('UCHAR')
					read(buffer, *, iostat=ios) Uchar
				case('LCHAR')
					read(buffer, *, iostat=ios) Lchar
				case('C_RATIO')
					read(buffer, *, iostat=ios) c_ratio
				case('CH_MULT')
					read(buffer, *, iostat=ios) ch_mult
				case('PR')
					read(buffer, *, iostat=ios) Pr
				case('VISC')
					read(buffer, *, iostat=ios) VISC
				case('COND')
					read(buffer, *, iostat=ios) COND
				case('B0')
					read(buffer, *, iostat=ios) B0
				case('INPUT_FILENAME')
					read(buffer, *, iostat=ios) fname_in
				case('BAPPLIED_FILENAME')
					read(buffer, *, iostat=ios) Bfname_in
				case('BOUNDARY_FILENAME')
					read(buffer, *, iostat=ios) bname_in
				case('HALLMHD')
					!read(buffer, *, iostat=ios) hall_string
					!HALLMHD = S_EQI(hall_string,'ON')
					HALLMHD = S_EQI(buffer,'ON')
				case('WRITE_FREQUENCY')
					read(buffer, *, iostat=ios) fnum_out
				case('INITIAL_OUT')
					initial_out = ((S_EQI(buffer,'YES')).or.(S_EQI(buffer,'Y')))
				case('MAX_TIME')
					read(buffer, *, iostat=ios) MAX_T
				case('CFL')
					read(buffer, *, iostat=ios) CFL
				case('CFL_H')
					read(buffer, *, iostat=ios) CFL_H
				case default
			if (myrank.eq.0) print *, 'Config. File: Invalid label at line ', line
				end select

			end if
		end do
		close(myhandle)

	end subroutine config

	Subroutine COURANT(cell,GAM,CK,TEMP_MEAN,VISC,gc,IX,&
	                   temp,DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,&
	                   BIX,BIY,BIZ,Bmu_0,vmax)
!-----------------------------------------------------------------------------
	Integer :: gc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
	                            XMOM, YMOM, ZMOM, ENER, BAX, BAY, BAZ, &
	                            BIX, BIY, BIZ, temp
	Double Precision :: VISCF


	vmax = 0.D0
	dtmax = 0.D0
	RGAMH = Dsqrt(GAM/2.D0)

	csq = 2.9979D8*2.9979D8       !Speed of light squared
        
	    Do i=1,IX
		bx = BIX(i) + BAX(i)
		by = BIY(i) + BAY(i)
		bz = BIZ(i) + BAZ(i)
		u = XMOM(i)/DENS(i)
		v = YMOM(i)/DENS(i)
		w = ZMOM(i)/DENS(i)

	      !  Fluid Velocity
	      eks=u**2 + v**2 + w**2
	      vel = Dsqrt(eks)

	      !  Alfven Velocity
	      Bmag = dsqrt(bx**2 + by**2 + bz**2)     
	      !valf = Bmag/dsqrt(Bmu_0*DENS(i,j,k))
	      valf = Bmag/dsqrt(Bmu_0*DENS(i))

	      !  Speed of Sound
	      !ELAM = (25.D-2)*(CK+3)*DENS(i,j,k)/(ENER(i,j,k)-(5.D-1)*eks*DENS(i,j,k))
	      ELAM = (25.D-2)*(CK+3)*DENS(i)/(ENER(i)-(5.D-1)*eks*DENS(i))
	      vsound=RGAMH*Dsqrt(1.D0/ELAM)

	      !  Magnetosonic Wave
	      vbs = dsqrt(csq*(vsound*vsound+valf*valf)/(csq+valf*valf))
		

	      vmax = MAX(vmax,vsound,vbs,vel,valf)
	    End Do
	
	Return
	End Subroutine COURANT    

	Subroutine COURANT_HALL(cell,GAM,CK,TEMP_MEAN,VISC,MOL_MASS,gc,IX,&
	                   temp,DENS,XMOM,YMOM,ZMOM,ENER,BAX,BAY,BAZ,&
	                   BIX,BIY,BIZ,Bmu_0,vmax)
!-----------------------------------------------------------------------------
	Integer :: gc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
	                            XMOM, YMOM, ZMOM, ENER, BAX, BAY, BAZ, &
	                            BIX, BIY, BIZ, temp
	Double Precision :: VISCF,MOL_MASS

	vmax = 0.D0
	dtmax = 0.D0
	RGAMH = Dsqrt(GAM/2.D0)
	R2h = 0.5D0/Cell
	Rh = 1/Cell

	csq = 2.9979D8*2.9979D8       !Speed of light squared
        
	    Do i=1,IX
		bx = BIX(i) + BAX(i)
		by = BIY(i) + BAY(i)
		bz = BIZ(i) + BAZ(i)
	      ! HALL
		! Whistler
	      Bmag = dsqrt(bx**2 + by**2 + bz**2)     
	      w_ci = (Q_elec*Avag)*Bmag/MOL_MASS       !Ion Cyclotron Frequency
		! from dispersion relation in CHen pp. 145 
		! assume omega<<omega_pe & omega<<omega_ce
	      vWW = MOL_MASS*Bmag*Rh/(Q_elec*Avag*Bmu_0*DENS(i))
	      !vWW = (valf**2)*Rh/w_ci
	      
	       ! Hall drift
	        dDENSdx = (DENS(i+1) - DENS(i-1))*R2h  !EP
	        dDENSdy = 0.D0
	        dDENSdz = 0.D0
	      	grad_rho = ABS(dDENSdx + dDENSdy + dDENSdz)
	      vHD = Bmag/(DENS(i)**2 * (Avag*Q_elec) * Bmu_0) * grad_rho * MOL_MASS

	      vmax = MAX(vmax,vWW,vHD)
	    End Do
	
	Return
	End Subroutine COURANT_HALL


END MODULE function_module
