#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 08:49:45 2021

@author: jsalazar
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

n=5
#filename=[]
mGKMData=[]
headernames=['i','rho','xMom','yMom','zMom','e','Bx','By','Bz']

nPlots=7
pickPlots=['rho','xMom','yMom','zMom','Bx','By','Bz'];


for i in range(n): 
    filename="field{:02d}.dat".format(i)
    mGKMData.append(pd.read_csv(filename,delim_whitespace=True,skiprows=0,header=0,names=headernames))


#ax=[None]*nPlots

for k in range(nPlots):
    #ax[k]=plt.axes(label=str(k))
    plt.figure()
    ax=plt.gca()
    plt.title(pickPlots[k])
    for i in range(n): 
        mGKMData[i].plot(x="i",y=pickPlots[k],ax=ax,label='t={}'.format(i))
        

        
