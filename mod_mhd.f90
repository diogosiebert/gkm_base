MODULE EM

	USE boundary_cond, ONLY : BOUNDARY_HALL
	USE constants
	USE WENO

IMPLICIT Double Precision (A-H,O-Z)


CONTAINS

	Subroutine MHD(Cell,gc,CK,IX,DENS,XMOM,YMOM,ZMOM,ENER,&
	               BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
		     BPSI,PSIBX,PSIBY,PSIBZ,&
		     FPSI,FBEVOX,FBEVOY,FBEVOZ,&
		     GPSI,GBEVOX,GBEVOY,GBEVOZ,&
		     HPSI,HBEVOX,HBEVOY,HBEVOZ,&
	               Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	               temp,kappa,TAU,&         
	               HALLMHD,vmax,c_ratio,ch_mult)
!--------------------------------------------------------------------------------
	Integer :: gc
	Double Precision :: Rh,R2h,Rhh,R4hh,Rmu,Rc,Rmuc,divB,divB0,divB_rel,divB_rel0,GAM,CK
	Double Precision :: Rhall, Rhalld, MOL_MASS, Q_elec, Avag, kappa, EPc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
			     XMOM,YMOM,ZMOM,temp,ENER,&
	                     BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
		 		    FPSI,FBEVOX,FBEVOY,FBEVOZ,&
		 		    GPSI,GBEVOX,GBEVOY,GBEVOZ,&
		 		    HPSI,HBEVOX,HBEVOY,HBEVOZ,&
				    BPSI,PSIBX,PSIBY,PSIBZ,&
				    BXT,BYT,BZT,QL,QR,&
				    BXL,BXR,BYL,BYR,BZL,BZR,&
				    XVEL,YVEL,ZVEL,XVELL,XVELR,YVELL,YVELR,ZVELL,ZVELR,PSIL,PSIR
	LOGICAL :: HALLMHD
	  
!-------Constants used in subroutine
	R3 = 1.D0/3.D0
	R6 = 1.D0/6.D0
	R12h = 1.D0/(12.D0*Cell)
	Rh=1.D0/(Cell)
	R2h=1.D0/(2.D0*Cell)
	R4h=1.D0/(4.D0*Cell)
	Rhh=1.D0/(Cell*Cell)
	R4hh=1.D0/(4.D0*Cell*Cell)
	Rmu=1.0D0/Bmu_0
	Rc=1.0D0/COND
	Rmuc=1.0D0/(Bmu_0*COND)
	Rhall = MOL_MASS/(Bmu_0*Q_elec*Avag)
	Rhallhd = Rhall*MOL_MASS/Avag
	EPc = kappa/Q_elec
	  
	divB=0.D0
	divB_rel=0.D0
	chsq = (ch_mult*vmax)**2
	cpsq = c_ratio*dsqrt(chsq+1.D-12) ! to avoid div by 0 -> ch_mult = 0 elimates PSI effects
	cpsqI = 1.D0/cpsq

! ###### FORM TOTAL MAGNETIC FIELD ########
	    Do i=1-gc,IX+gc
		BXT(i) = BAX(i) + BIX(i)
		BYT(i) = BAY(i) + BIY(i)
		BZT(i) = BAZ(i) + BIZ(i)
		XVEL(i) = XMOM(i)/DENS(i)
		YVEL(i) = YMOM(i)/DENS(i)
		ZVEL(i) = ZMOM(i)/DENS(i)
	    End do
	    Do i=1,IX

	      u = XVEL(i)
	      v = YVEL(i)
	      w = ZVEL(i)
	      bx = BXT(i)
	      by = BYT(i)
	      bz = BZT(i)
! ######### CALCULATE DERIVATIVES ###############

   !#########################################
   !#########################################
	! DERIVATIVES FROM UNINTERPOLATED VARS
		! FIRST DERIVATIVES - Central, O(dx^2)
	      	dBXdx = (BXT(i+1) - BXT(i-1))*R2h
	      	dBYdx = (BYT(i+1) - BYT(i-1))*R2h
	      	dBZdx = (BZT(i+1) - BZT(i-1))*R2h

	      	dBXdy = 0.D0
	      	dBYdy = 0.D0
	      	dBZdy = 0.D0

	      	dBXdz = 0.D0
	      	dBYdz = 0.D0
	      	dBZdz = 0.D0

	      	dudx = (XVEL(i+1) - XVEL(i-1))*R2h
	      	dvdx = (YVEL(i+1) - YVEL(i-1))*R2h
	      	dwdx = (ZVEL(i+1) - ZVEL(i-1))*R2h

	      	dudy = 0.D0
	      	dvdy = 0.D0
	      	dwdy = 0.D0

	      	dudz = 0.D0
	      	dvdz = 0.D0
	      	dwdz = 0.D0

	        dTdx = (temp(i+1) - temp(i-1))*R2h  !EP
	        dTdy = 0.D0
	        dTdz = 0.D0

	        dDENSdx = (DENS(i+1) - DENS(i-1))*R2h  !EP
	        dDENSdy = 0.D0
	        dDENSdz = 0.D0
		
		! Second Derivatives O(dx^2)
		dBXdxdx = (BXT(i+1)-2.D0*BXT(i)+BXT(i-1))*Rhh
	        dBYdydx =0.D0
	        dBZdzdx = 0.D0

	        dBXdxdy = 0.D0
		dBYdydy = 0.D0
	        dBZdzdy = 0.D0

	        dBXdxdz = 0.D0
	        dBYdydz = 0.D0
		dBZdzdz = 0.D0

		dBYdxdx = (BYT(i+1)-2.D0*BYT(i)+BYT(i-1))*Rhh
		dBZdxdx = (BZT(i+1)-2.D0*BZT(i)+BZT(i-1))*Rhh
		dBXdydy = 0.D0
		dBZdydy = 0.D0
		dBXdzdz = 0.D0
		dBYdzdz = 0.D0
		
		divB = dBXdx+dBYdy+dBZdz


! ######## MHD SOURCE TERM CALCULATIONS ############
!--------Contributions to Momentum
	      BXM(i) = Rmu*((dBXdz-dBZdx)*bz&
	                   -(dBYdx-dBXdy)*by)

	      BYM(i) = -Rmu*((dBZdy-dBYdz)*bz&
	                   -(dBYdx-dBXdy)*bx)
			
	      BZM(i) = Rmu*((dBZdy-dBYdz)*by&
	                   -(dBXdz-dBZdx)*bx)
!--------Contributions to Energy
	      !BEN_EP =   -EPc*Rmu/DENS(i) * ( &     !Electron Pressure: Energy
	                 !(DENS(i)*dTdx + temp(i)*dDENSdx) * &
						!(dBZdy-dBYdz) + &
	                 !(DENS(i)*dTdy + temp(i)*dDENSdy) * &
	                                        !(dBXdz-dBZdx) + &
	                 !(DENS(i)*dTdz + temp(i)*dDENSdz) * &
	                                        !(dBYdx-dBXdy)) 
			
	!resistive
	      BEN(i) = Rc*(Rmu**2)*((dBZdy-dBYdz)**2&
	                   +(dBZdx-dBXdz)**2&
	                   +(dBYdx-dBXdy)**2)&
	                   +(Rmu)*(u*((dBXdz-dBZdx)*bz&
	                   -(dBYdx-dBXdy)*by)&
	                   -v*((dBZdy-dBYdz)*bz&
	                   -(dBYdx-dBXdy)*bx)&
	                   +w*((dBZdy-dBYdz)*by&
	                   -(dBXdz-dBZdx)*bx))! +&
	                   !BEN_EP

!--------PSI - divB control
	      PSIBX(i) = -u*divB
	      PSIBY(i) = -v*divB
	      PSIBZ(i) = -w*divB
	      !BPSI(i) = -chsq*cpsqI*PSI(i)
	      BPSI(i) = 0.D0

	!---- Resistive source terms for mag field
	      BEVOX(i) = -Rmuc*(dBYdydx+dBZdzdx-dBXdydy-dBXdzdz)

	      BEVOY(i) = -Rmuc*(dBXdxdy+dBZdzdy-dBYdxdx-dBYdzdz)
			
	      BEVOZ(i) = -Rmuc*(dBXdxdz+dBYdydz-dBZdxdx-dBZdydy)
	    End do

!! ###################################################
!! ############# NEW STUFF FOR FLUXES ################
!! -------------------------------------------------
 !!################################################
 !!########## Calculations using H-monotone flux
 !!######### THESE FLUXES made from interpolated vars
 !!######## f = h(uMinus,uPlus) = 0.5(f(uMinus)+f(uPlus)-a(uPlus-uMinus))

	! X-FLUX
Call weno_x(gc,IX,BXT,BXL,BXR)
Call weno_x(gc,IX,BYT,BYL,BYR)
Call weno_x(gc,IX,BZT,BZL,BZR)
Call weno_x(gc,IX,XVEL,XVELL,XVELR)
Call weno_x(gc,IX,YVEL,YVELL,YVELR)
Call weno_x(gc,IX,ZVEL,ZVELL,ZVELR)
Call weno_x(gc,IX,PSI,PSIL,PSIR)
	    Do i=0,IX
	      	dBXdx = (BXT(i+1) - BXT(i))*Rh
	      	dBXdy = 0.D0
	      	dBXdz = 0.D0
		
		u = XVELL(i+1)
		v = YVELL(i+1)
		w = ZVELL(i+1)
		bx = BXL(i+1)
		by = BYL(i+1)
		bz = BZL(i+1)
		psi_temp = PSIL(i+1)
		FBEVOX(i) = psi_temp
		FBEVOY(i) = (by*u-v*bx)
		FBEVOZ(i) = (bz*u-w*bx)
		FPSI(i) = chsq*bx
		u = XVELR(i+1)
		v = YVELR(i+1)
		w = ZVELR(i+1)
		bx = BXR(i+1)
		by = BYR(i+1)
		bz = BZR(i+1)
		psi_temp = PSIR(i+1)

		FBEVOX(i) = 0.5D0*(FBEVOX(i) + psi_temp - &
					vmax*(BXR(i+1)-BXL(i+1)))
		FBEVOY(i) = 0.5D0*(FBEVOY(i) +(by*u-v*bx) - &
					vmax*(BYR(i+1)-BYL(i+1)))
		FBEVOZ(i) = 0.5D0*(FBEVOZ(i) +(bz*u-w*bx) - &
					vmax*(BZR(i+1)-BZL(i+1)))
		FPSI(i) = 0.5D0*(FPSI(i) +chsq*bx - &
		   vmax*(PSIR(i+1)-PSIL(i+1)))
	    End do

	 Return
	 End Subroutine MHD



!--------------------------------------------------------------------------------
	Subroutine Hall_MHD(Cell,gc,IX,DENS,XMOM,YMOM,ZMOM,ENER,&
	               BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,BEVOX,BEVOY,BEVOZ,&
		       FBEVOX,FBEVOY,FBEVOZ,GBEVOX,GBEVOY,GBEVOZ,HBEVOX,HBEVOY,HBEVOZ,&
		       BPSI,PSIBX,PSIBY,PSIBZ,FPSI,GPSI,HPSI,&
	               Bmu_0,COND,B0,U_wall,IT,U_in,MOL_MASS,Avag,Q_elec,&
	               temp,kappa,vmaxh,c_ratio,ch_mult)
!--------------------------------------------------------------------------------

	Integer :: gc
	Double Precision :: Rh,R2h,Rhh,R4hh,Rmu,Rc,Rmuc,divB,divB0,divB_rel,divB_rel0
	Double Precision :: Rhall, Rhalld, MOL_MASS, Q_elec, Avag, kappa, EPc
	Double Precision, Dimension(1-gc:IX+gc) :: DENS,&
				    XMOM,YMOM,ZMOM,ENER,&
	                            BAX,BAY,BAZ,BIX,BIY,BIZ,PSI,BXM,BYM,BZM,BEN,&
				    BEVOX,BEVOY,BEVOZ,&
		                    FBEVOX,FBEVOY,FBEVOZ,GBEVOX,GBEVOY,&
				    GBEVOZ,HBEVOX,HBEVOY,HBEVOZ,FPSI,GPSI,HPSI,&
				    BPSI,PSIBX,PSIBY,PSIBZ,&
				   BXT,BYT,BZT,BXL,BXR,BYL,BYR,BZL,BZR,temp,DENSL,DENSR,PSIL,PSIR
	  
!-------Constants used in subroutine
	Rh=1.D0/(Cell)
	R2h=1.D0/(2.D0*Cell)
	Rhh=1.D0/(Cell*Cell)
	R12h = 1.D0/(12.D0*Cell)
	R4hh=1.D0/(4.D0*Cell*Cell)
	Rmu=1.0D0/Bmu_0
	Rc=1.0D0/COND
	Rmuc=1.0D0/(Bmu_0*COND)
	Rhall = MOL_MASS/(Bmu_0*Q_elec*Avag)
	Rhallhd = Rhall*MOL_MASS/Avag
	EPc = kappa/Q_elec
	  
	divB=0.D0
	divB_rel=0.D0
	chsq = (ch_mult*vmaxh)**2
	cpsq = c_ratio*vmaxh
	cpsqI = 1.D0/cpsq
! ###### FORM TOTAL MAGNETIC FIELD ########
	    Do i=1-gc,IX+gc
		BXT(i) = BAX(i) + BIX(i)
		BYT(i) = BAY(i) + BIY(i)
		BZT(i) = BAZ(i) + BIZ(i)
	    End do
	    Do i=1,IX
		u = XMOM(i)/DENS(i)
		v = YMOM(i)/DENS(i)
		w = ZMOM(i)/DENS(i)

	      	dBXdx = (BXT(i+1) - BXT(i-1))*R2h
	      	dBYdy = 0.D0
	      	dBZdz = 0.D0
		divB = dBXdx + dBYdy + dBZdz

!--------PSI - divB control
	      PSIBX(i) = -u*divB
	      PSIBY(i) = -v*divB
	      PSIBZ(i) = -w*divB
	      BPSI(i) = -chsq*cpsqI*PSI(i)

	!---- Resistive source terms for mag field
	      BEVOX(i) = 0.D0

	      BEVOY(i) = 0.D0
			
	      BEVOZ(i) = 0.D0
	    End do
	  !End do
	!End do
!! ###################################################
!! ############# NEW STUFF FOR FLUXES ################
!! -------------------------------------------------
 !!################################################
 !!########## Calculations using H-monotone flux
 !!######### THESE FLUXES made from interpolated vars
 !!######## f = h(uMinus,uPlus) = 0.5(f(uMinus)+f(uPlus)-a(uPlus-uMinus))

	! X-FLUX
Call weno_x(gc,IX,BXT,BXL,BXR)
Call weno_x(gc,IX,BYT,BYL,BYR)
Call weno_x(gc,IX,BZT,BZL,BZR)
Call weno_x(gc,IX,DENS,DENSL,DENSR)
Call weno_x(gc,IX,PSI,PSIL,PSIR)
	    Do i=0,IX
	      	dBYdx = (BYT(i+1) - BYT(i))*Rh
	      	dBZdx = (BZT(i+1) - BZT(i))*Rh
	      	dBXdy = 0.D0
	      	dBZdy = 0.D0
	      	dBXdz = 0.D0
	      	dBYdz = 0.D0

		Uhx = (dBYdz-dBZdy)
		Uhy = (dBZdx-dBXdz)
		Uhz = (dBXdy-dBYdx)
		if (Uhx.ge.0.D0) then
		  r_rho = 1.D0/DENSL(i+1)
		  bx = BXL(i+1)
		  by = BYL(i+1)
		  bz = BZL(i+1)
		else if(Uhx.lt.0.D0) then
		  r_rho = 1.D0/DENSR(i+1)
		  bx = BXR(i+1)
		  by = BYR(i+1)
		  bz = BZR(i+1)
		end if
		Vhx = Uhx*Rhall*r_rho
		Vhy = Uhy*Rhall*r_rho
		Vhz = Uhz*Rhall*r_rho

	! or is it the opposite sign?? There is some confusion :(
		FBEVOX(i) = 0.5D0*(PSIL(i+1) + PSIR(i+1) - &
			vmaxh*(BXR(i+1) - BXL(i+1)))
		FBEVOY(i) = (by*Vhx-Vhy*bx)
		FBEVOZ(i) = (bz*Vhx-Vhz*bx)
		FPSI(i) = 0.5D0*(chsq*BXL(i+1) + chsq*BXR(i+1) - &
			vmaxh*(PSIR(i+1) - PSIL(i+1)))
	    End do

	 Return
	 End Subroutine Hall_MHD

END MODULE EM
